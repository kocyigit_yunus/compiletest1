//
//  ViewController.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 29/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    //var viewController1 : ViewController1!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationController = UINavigationController()
        self.addChildViewController(navigationController)
        self.view.addSubview(navigationController.view)
        navigationController.view.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        //let viewController2: ViewController2 = ViewController2()
        //navigationController.viewControllers = [viewController2]
        
        //let viewController4: ViewController4 = ViewController4()
        //navigationController.viewControllers = [viewController4]
        
        //let viewController5: ViewController5 = ViewController5()
        //navigationController.viewControllers = [viewController5]
        
        //let plainPickerViewController: PlainPickerViewController = PlainPickerViewController()
        //navigationController.viewControllers = [plainPickerViewController]
        
        //let plainPickerTestViewController: PlainPickerTestViewController = PlainPickerTestViewController()
        //navigationController.viewControllers = [plainPickerTestViewController]
        
        //let stackViewTestViewController: StackViewTestViewController = StackViewTestViewController ()
        //navigationController.viewControllers = [stackViewTestViewController]
        
        //let plainTextFieldController: PlainTextFieldViewController = PlainTextFieldViewController()
        //navigationController.viewControllers = [plainTextFieldController]
        
        //let amountTextFieldTestController: AmountTextFieldViewController = AmountTextFieldViewController()
        //navigationController.viewControllers = [amountTextFieldTestController]
        
        let dovizKurlariViewController: DovizKurlariViewController = DovizKurlariViewController()
        navigationController.viewControllers = [dovizKurlariViewController]
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

