//
//  ActionButton.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 01/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class ActionButton: UIControl {
    
    var minHeight: CGFloat { get { return 56 } }
    
    private var lblTitle: UILabel!
    var text: String! = "DEVAM" { didSet { lblTitle.text = text } }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func didLoad(){
        
        lblTitle = UILabel(frame: CGRect.zero)
        lblTitle.text = text
        lblTitle.font = UIFont.brandonBold14
        //lblTitle.backgroundColor = Color.color1LightGreen
        lblTitle.textColor = UIColor.white
        self.addSubview(lblTitle)
        
        self.backgroundColor = UIColor.ceptetebShamrockGreenColor()
        remakeConstraints()
        
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    func remakeConstraints(){
        lblTitle.snp.remakeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }
    
    
    func keyboardWillShown(notification: NSNotification){
        if(self.superview != nil){ self.superview?.bringSubview(toFront: self) }
        updateBottomConstraint(show: true, notification: notification)
    }
    
    func keyboardWillHide(notification: NSNotification){
        updateBottomConstraint(show: false, notification: notification)
    }
    
    func updateBottomConstraint(show: Bool, notification: NSNotification){
        var bottomConstraint: NSLayoutConstraint? = nil
        
        for constraint in self.superview!.constraints {
            if(constraint.firstAttribute == .bottom && constraint.secondAttribute == .bottom){
                if(constraint.firstItem as! UIView == self || constraint.secondItem as! UIView == self){
                    bottomConstraint = constraint
                    break
                }
            }
        }
        
        let userInfo = notification.userInfo!
        let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        
        if( show ){
            bottomConstraint?.constant = -(endFrame?.size.height ?? 0)
        }else{
            bottomConstraint?.constant = 0.0
        }
        
        UIView.animate(withDuration: 0.2,
                       animations: { self.superview?.layoutIfNeeded() },
                       completion: nil)
        
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        self.removeTarget(nil, action: nil, for: .allEvents)
    }
}
