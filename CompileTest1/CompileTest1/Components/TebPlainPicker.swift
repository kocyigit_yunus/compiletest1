//
//  TebTextFieldType1.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

public class TebPlainPicker: TebBaseComponent {
    
    // TebPlainPicker
    
    var selectedItemLabel: UILabel!
    var titleLabel: UILabel!
    var lineView: UIView!
    var invalidDescriptionLabel: UILabel!
    private var lastWidth: CGFloat = -10000
    
    // MARK: Validatable
    
    override public var errorMessage: String? {
        didSet {
            self.invalidDescriptionLabel.text = errorMessage
        }
    }
    
    override public func validate() {
        isValid = true
        errorMessage = nil
        self.validateState = .onChange
        
        if(checkValidation != true){
            return
        }
        
        for validator in validators!.sorted( by: { $0.priority > $1.priority }) {
            
            var potentialErrorMessage: String? = nil
            potentialErrorMessage = validator.validate(value: ( self.selectedItemLabel.text ) as AnyObject? )
            
            if(potentialErrorMessage != nil){
                errorMessage = potentialErrorMessage
                isValid = false
                return
            }
        }
        
    }
    
    override public func updateComponentStateForValidation() {
        if( !self.isValid ){
            // decide if .emptyInvalid or .filledInvalid
            if( self.selectedItemLabel.text == nil ){
                updateComponentState(newState: .emptyInvalid, animated: false)
            }else{
                updateComponentState(newState: .filledInvalid, animated: false)
            }
        }
    }
    
    override func didLoad(){
        super.didLoad()
        self.setContentCompressionResistancePriority(1000, for: .vertical)
        
        titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.font = UIFont.brandonRegular16
        titleLabel.textColor = UIColor.ceptetebDark40Color()
        titleLabel.text = titleTextEmpty
        if(DevelopmentHelper.markComponentLevel2){ titleLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(titleLabel)
        
        selectedItemLabel = UILabel(frame: CGRect.zero)
        selectedItemLabel.font = UIFont.brandonRegular16
        selectedItemLabel.textColor = UIColor.ceptetebDark60Color()
        selectedItemLabel.text = nil
        if(DevelopmentHelper.markComponentLevel2){ selectedItemLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(selectedItemLabel)
        
        lineView = UIView(frame: CGRect.zero)
        lineView.backgroundColor = UIColor.ceptetebDark40Color()
        self.addSubview(lineView)
        
        invalidDescriptionLabel = UILabel(frame: CGRect.zero)
        invalidDescriptionLabel.font = UIFont.brandonRegular14
        invalidDescriptionLabel.textColor = self.colorInvalid
        invalidDescriptionLabel.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ invalidDescriptionLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(invalidDescriptionLabel)
        
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
        
        updateProperties(animated: false)
    }
    
    override func updateProperties(animated: Bool){
        super.updateProperties(animated: animated)
        
        if( self.componentState == .emptyDisabled ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorDisabled
            self.lineView.backgroundColor = self.colorDisabled
        }else if( self.componentState == .empty ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorEmpty
        }else if( self.componentState == .emptyInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorEmpty
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .editing ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextEditing
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorFiled
        }else if( self.componentState == .editingInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .filledInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .filled ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextFilled
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorEmpty
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }
        
    }
    
    // MARK: Layout
    
    override open var intrinsicContentSize : CGSize {
        print("intrinsicContentSize with state : +++\(componentState)+++")
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let targetedSize: CGSize = CGSize(width: self.frame.size.width, height: 10000)
        if( componentState == .empty || componentState == .emptyDisabled || componentState == .filled || componentState == .editing ){
            let calculatedSize = CGSize(width: targetedSize.width, height:72)
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }
        else if( componentState == .emptyInvalid || componentState == .filledInvalid || componentState == .editingInvalid ){
            let errorRect = self.invalidDescriptionLabel.sizeThatFits(CGSize(width: targetedSize.width-paddingLeft-paddingRight, height: 10000 ))
            let calculatedSize = CGSize(width: targetedSize.width, height:55+2+3+errorRect.height+5)
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }
    }
    
    override public func layoutSubviews() {
        print("layoutSubviews with state : +++\(componentState)+++")
        let width: CGFloat = self.frame.size.width
        if( width != lastWidth ){
            lastWidth = width
            delay(0.0){
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
            }
        }
        let lineHeight: CGFloat = componentState == .emptyInvalid || componentState == .filledInvalid ? 2 : 1
        
        var leftImageFrame: CGRect = CGRect.zero
        var rightImageFrame: CGRect = CGRect.zero
        
        if(leftImageName != nil){
            if(paddingLeft == 16){ paddingLeft = 10 }
            leftImageFrame = CGRect(x: 0, y: 0, width: 36, height: 36)
            leftImageFrame = CGRect(x: paddingLeft, y: 0, width: 36, height: 36)
        }
        if(rightImageName != nil) {
            rightImageFrame = CGRect(x: 0, y: 0, width: 24, height: 24)
        }
        
        var usableWidth: CGFloat = leftImageFrame.width == 0 ? width-paddingLeft-paddingRight : width-62-paddingLeft-paddingRight
        let lineWidth: CGFloat = usableWidth
        usableWidth = rightImageFrame.width == 0 ? usableWidth : usableWidth-24
        
        let placeholderSize = self.titleLabel.sizeThatFits( CGSize(width: usableWidth, height:10000) )
        let selectedItemLabelSize = CGSize(width: usableWidth, height: selectedItemLabel.sizeThatFits( CGSize(width:10000, height:10000)).height )
        let invalidDescriptionSize = self.invalidDescriptionLabel.sizeThatFits( CGSize(width: lineWidth, height:10000) )
        
        let leftSpace: CGFloat = leftImageFrame.width == 0 ? paddingLeft : 62 + paddingLeft
        
        /*
         .empty => 1
         .emptyInvalid => 2
         .editing => 4
         .editingInvalid => 3
         .filled => 4
         .filledInvalid => 3
         */
        
        switch componentState {
        case .empty, .emptyDisabled:
            titleLabel.frame = CGRect(x: leftSpace, y: 27, width: placeholderSize.width, height: placeholderSize.height)
            titleLabel.center.y = 35
            selectedItemLabel.frame = CGRect(x: leftSpace, y: 27, width: selectedItemLabelSize.width, height: selectedItemLabelSize.height)
            selectedItemLabel.center.y = titleLabel.center.y
            lineView.frame = CGRect(x: leftSpace, y:titleLabel.frame.maxY+5, width: lineWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y:lineView.frame.maxY+3, width: usableWidth, height: 0)
        case .emptyInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 27, width: placeholderSize.width, height: placeholderSize.height)
            titleLabel.center.y = 35
            selectedItemLabel.frame = CGRect(x: leftSpace, y: 27, width: selectedItemLabelSize.width, height: selectedItemLabelSize.height)
            selectedItemLabel.center.y = titleLabel.center.y
            lineView.frame = CGRect(x: leftSpace, y:titleLabel.frame.maxY+5, width: lineWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
        case .editingInvalid, .filledInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: placeholderSize.width, height: placeholderSize.height)
            selectedItemLabel.frame = CGRect(x: leftSpace, y: 27, width: selectedItemLabelSize.width, height: selectedItemLabelSize.height)
            lineView.frame = CGRect(x: leftSpace, y:selectedItemLabel.frame.maxY+5, width: lineWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
        case .editing, .filled:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: placeholderSize.width, height: placeholderSize.height)
            selectedItemLabel.frame = CGRect(x: leftSpace, y: 27, width: selectedItemLabelSize.width, height: selectedItemLabelSize.height)
            lineView.frame = CGRect(x: leftSpace, y:selectedItemLabel.frame.maxY+5, width: lineWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y:lineView.frame.maxY+3, width: usableWidth, height: 0)
        default:
            fatalError("unrecognized state on layoutSubviews")
        }
        
        leftImageFrame.origin.y = lineView.frame.origin.y - leftImageFrame.size.height
        leftImageFrame.origin.y = leftImageFrame.origin.y + leftImageCenterYOffset
        leftImageFrame.origin.x = leftImageFrame.origin.x + leftImageCenterXOffset
        
        rightImageFrame.origin.x = width-paddingRight-rightImageFrame.size.width
        rightImageFrame.origin.y = lineView.frame.origin.y - rightImageFrame.size.height - 7
        rightImageFrame.origin.y = rightImageFrame.origin.y + rightImageCenterYOffset
        rightImageFrame.origin.x = rightImageFrame.origin.x + rightImageCenterXOffset
        
        leftImageView.frame = leftImageFrame
        rightImageView.frame = rightImageFrame
        
    }
    
    override public var debugDescription: String {
        get {
            return "component state : \(self.componentState) \(super.debugDescription)"
        }
    }
    
    deinit {
        // remove all targets and notifications
    }
}
