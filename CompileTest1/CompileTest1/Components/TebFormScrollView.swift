//
//  TebFormScrollView.swift
//  Cepteteb
//
//  Created by Yunus Koçyiğit on 16/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation
import UIKit

public class TebFormScrollView: UIScrollView {
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }
    
    convenience public  init() {
        self.init(frame: CGRect.zero)
        didLoad()
    }
    
    public func isValid() -> Bool {
        
        self.validateSubviews()
        let invalidSubview = self.deepestFirstInvalidSubview()
        
        if( invalidSubview != nil ){
            
            UIView.animate(withDuration: 0.3) {
                self.updateSubviewValidationStates()
                self.layoutIfNeeded()
                let yPositionOfInvalidSubview = self.findYPositionForView(targetView: invalidSubview!)
                self.setContentOffset(CGPoint(x: 0, y: yPositionOfInvalidSubview), animated: false)
            }
            
        }
        
        return invalidSubview != nil ? false : true
        
    }
    
    public func didLoad(){
        keyboardDismissMode = .onDrag
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToActiveTextField(notification:)), name:NSNotification.Name.UITextFieldTextDidBeginEditing, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.scrollToActiveTextField(notification:)), name:NSNotification.Name.UITextViewTextDidBeginEditing, object: nil)
        
    }
    
    public func scrollToActiveTextField( notification: Notification ){
        
        delay(0.01){
            
            var viewToScroll = self.findFirstResponderBeneathView(targetView: self)
            
            if(viewToScroll?.superview != nil && viewToScroll?.superview is TebBaseComponent){
                viewToScroll = viewToScroll?.superview
            }
            
            if(viewToScroll != nil){
                
                UIView.animate(withDuration: 0.2, animations: {
                    let yToScroll = self.findYPositionForView(targetView:  viewToScroll!)
                    self.setContentOffset(CGPoint(x: 0, y: yToScroll), animated: false)
                })
                
            }
            
        }
        
    }
    
    public func findFirstResponderBeneathView(targetView: UIView) -> UIView?{
        for subview in targetView.subviews {
            if( subview.responds(to: #selector(getter: UIResponder.isFirstResponder) ) && subview.isFirstResponder ){
                return subview
            }else{
                let result: UIView? = self.findFirstResponderBeneathView(targetView: subview)
                if( result != nil ) { return result }
            }
        }
        return nil
    }
    
    
    public func findYPositionForView(targetView : UIView) -> CGFloat {
        
        var closestSubview = self.findClosestParentOfView(targetView: targetView)
        if( closestSubview is UIStackView || closestSubview != nil /* content view uiview yada stack view olabilir */ ){
            closestSubview = closestSubview?.findClosestParentOfView(targetView: targetView)
            
            // scroll view in frame inin height i
            let scrollHeight: CGFloat = self.frame.size.height
            
            // scrollanmak istenen alanın y koordinatı
            let subviewY: CGFloat = closestSubview!.frame.origin.y
            
            // scrol lview in contentinin height i ve content insetleri
            let contentHeight: CGFloat = self.contentSize.height
            let contentInsetTop: CGFloat = self.contentInset.top
            let contentInsetBottom: CGFloat = self.contentInset.bottom
            
            // y ekseninde scrollanılabilecek maksimum deger
            let scrollableMax = max(0,contentHeight-scrollHeight+contentInsetTop+contentInsetBottom)
            
            // y ekseninde scrollanılacak değer yukarıdan ne kadar altta olmalı
            let marginForScroll: CGFloat = 0
            
            // scrollanılacak değerin content inset top hesaplanmadan önceki hali
            let scrollTo = min(scrollableMax, subviewY-marginForScroll)
            
            // scrollanılacak gerçek değer :)
            let willReturn = scrollTo-contentInsetTop
            
            print("will return : \(willReturn)")
            
            return willReturn
        }
        
        return -1
        
    }
    
    override public var debugDescription: String {
        get {
            return "content inset : \(self.contentInset) \(super.debugDescription)"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
