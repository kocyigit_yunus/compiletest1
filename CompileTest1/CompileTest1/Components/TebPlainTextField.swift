//
//  TebTextFieldType1.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

public class TebPlainTextField: TebBaseComponent, UITextFieldDelegate {
    
    // TebPlainTextField
    
    public var inputTextField: UITextField!
    var titleLabel: UILabel!
    var lineView: UIView!
    var invalidDescriptionLabel: UILabel!
    public var descriptionText: String? {
        didSet {
            if( self.componentState != .emptyInvalid || self.componentState != .filledInvalid || self.componentState != .editingInvalid ) {
                self.invalidDescriptionLabel.text = descriptionText
                self.doLayoutJobs()
            }
        }
    }
    public weak var tebTextFieldDelegate: TebTextFieldDelegate?
    private var lastWidth: CGFloat = -100000
    public override var isEnabled: Bool {
        didSet {
            if(isEnabled){
                if( self.inputTextField.text != nil ){
                    self.updateComponentState(newState: .filled, animated: false)
                }else{
                    self.updateComponentState(newState: .empty, animated: false)
                }
            }else{
                if( self.inputTextField.text != nil ){
                    self.updateComponentState(newState: .filledDisabled, animated: false)
                }else{
                    self.updateComponentState(newState: .emptyDisabled, animated: false)
                }
            }
        }
    }
    
    // MARK: Validatable
    override public var errorMessage: String? {
        didSet {
            //self.invalidDescriptionLabel.text = errorMessage
        }
    }
    
    override public func validate() {
        isValid = true
        errorMessage = nil
        self.validateState = .onChange
        
        if(checkValidation != true){
            return
        }
        
        for validator in validators!.sorted( by: { $0.priority > $1.priority }) {
            
            var potentialErrorMessage: String? = nil
            if( validator is MinLengthValidator ){
                potentialErrorMessage = validator.validate(value: ( self.inputTextField.text ) as AnyObject? )
            }else{
                potentialErrorMessage = validator.validate(value: ( self.inputTextField.text ) as AnyObject? )
            }
            
            if(potentialErrorMessage != nil){
                errorMessage = potentialErrorMessage
                isValid = false
                return
            }else{
                errorMessage = nil
            }
        }
        
    }
    
    override public func updateComponentStateForValidation() {
        if( !self.isValid ){
            // decide if .emptyInvalid or .filledInvalid
            if( self.inputTextField.text != nil && self.inputTextField.text!.characters.count > 0 ){
                updateComponentState(newState: .filledInvalid, animated: false)
            }else{
                updateComponentState(newState: .emptyInvalid, animated: false)
            }
        }
    }
    
    // MARK: Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if( self.componentState == .emptyInvalid || self.componentState == .editingInvalid ){
            self.updateComponentState(newState: .editingInvalid, animated: true)
            tebTextFieldDelegate?.tebTextField?(editingDidBegin: self.inputTextField)
            return
        }
        
        self.updateComponentState(newState: .editing, animated: true)
        tebTextFieldDelegate?.tebTextField?(editingDidBegin: self.inputTextField)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.validateState = .onChange
        self.resignFirstResponder()
        
        self.validate()
        if( self.isValid ) {
            if(textField.text != nil &&  textField.text!.characters.count > 0){
                self.updateComponentState(newState: .filled, animated: true)
            }else{
                self.updateComponentState(newState: .empty, animated: true)
            }
        }else{
            if(textField.text != nil &&  textField.text!.characters.count > 0){
                self.updateComponentState(newState: .filledInvalid, animated: true)
            }else{
                self.updateComponentState(newState: .emptyInvalid, animated: true)
            }
        }
        tebTextFieldDelegate?.tebTextField?(editingDidEnd: self.inputTextField)
    }
    
    func textFieldDidChange(sender: UITextField?){
        if( self.componentState != .editing ){
            self.updateComponentState(newState: .editing, animated: true)
        }
        if( self.validateState == .onChange ){
            self.validate()
            if( self.isValid ){
                // valid
                if( self.componentState != .editing ){
                    self.updateComponentState(newState: .editing, animated: true)
                }
            }else{
                // invalid
                if( self.componentState != .editingInvalid ){
                    self.updateComponentState(newState: .editingInvalid, animated: true)
                }
            }
        }
        tebTextFieldDelegate?.tebTextField?(editingChanged: self.inputTextField)
    }
    
    override func didLoad(){
        super.didLoad()
        self.setContentCompressionResistancePriority(1000, for: .vertical)
        
        titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.font = UIFont.brandonRegular16
        titleLabel.textColor = UIColor.ceptetebDark40Color()
        titleLabel.numberOfLines = 0
        titleLabel.text = titleTextEmpty
        if(DevelopmentHelper.markComponentLevel2){ titleLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(titleLabel)
        
        lineView = UIView(frame: CGRect.zero)
        lineView.backgroundColor = UIColor.ceptetebDark40Color()
        self.addSubview(lineView)
        
        inputTextField = UITextField(frame: CGRect.zero)
        inputTextField.font = UIFont.brandonRegular16
        inputTextField.textColor = UIColor.ceptetebDark40Color()
        inputTextField.delegate = self
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        if(DevelopmentHelper.markComponentLevel2){ inputTextField.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(inputTextField)
        
        invalidDescriptionLabel = UILabel(frame: CGRect.zero)
        invalidDescriptionLabel.font = UIFont.brandonRegular14
        invalidDescriptionLabel.textColor = self.colorInvalid
        invalidDescriptionLabel.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ invalidDescriptionLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(invalidDescriptionLabel)
        
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
        
        updateProperties(animated: false)
    }
    
    override func updateProperties(animated: Bool){
        super.updateProperties(animated: animated)
        
        if(self.componentState == .emptyDisabled ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextDisabled
            self.titleLabel.textColor = self.colorDisabled
            self.lineView.backgroundColor = self.colorDisabled
            self.invalidDescriptionLabel.textColor = self.colorDisabled
            self.invalidDescriptionLabel.text = descriptionText
        }else if(self.componentState == .filledDisabled ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextDisabled
            self.titleLabel.textColor = self.colorDisabled
            self.lineView.backgroundColor = self.colorDisabled
            self.invalidDescriptionLabel.textColor = self.colorEmpty
            self.invalidDescriptionLabel.text = descriptionText
        }else if( self.componentState == .empty ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorEmpty
            self.lineView.backgroundColor = self.colorEmpty
            self.invalidDescriptionLabel.textColor = self.colorEmpty
            self.invalidDescriptionLabel.text = descriptionText
        }else if( self.componentState == .emptyInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorEmpty
            self.lineView.backgroundColor = self.colorInvalid
            self.invalidDescriptionLabel.textColor = self.colorInvalid
            self.invalidDescriptionLabel.text = errorMessage
        }else if( self.componentState == .editing ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextEditing
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorFiled
            self.invalidDescriptionLabel.textColor = self.colorEmpty
            self.invalidDescriptionLabel.text = descriptionText
        }else if( self.componentState == .editingInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
            self.invalidDescriptionLabel.textColor = self.colorInvalid
            self.invalidDescriptionLabel.text = errorMessage
        }else if( self.componentState == .filledInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
            self.invalidDescriptionLabel.textColor = self.colorInvalid
            self.invalidDescriptionLabel.text = errorMessage
        }else if( self.componentState == .filled ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextFilled
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorFiled
            self.invalidDescriptionLabel.textColor = self.colorEmpty
            self.invalidDescriptionLabel.text = descriptionText
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }

        
    }
    
    // MARK: Layout
    
    
    override open var intrinsicContentSize : CGSize {
        print("intrinsicContentSize with state : +++\(componentState)+++")
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let targetedSize: CGSize = CGSize(width: self.frame.size.width, height: 10000)
        if( componentState == .empty || componentState == .emptyDisabled || componentState == .filledDisabled || componentState == .filled || componentState == .editing ){
            let calculatedSize = CGSize(width: targetedSize.width, height: invalidDescriptionLabel.frame.maxY + 13 )
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }
        else if( componentState == .emptyInvalid || componentState == .filledInvalid || componentState == .editingInvalid ){
            let calculatedSize = CGSize(width: targetedSize.width, height: invalidDescriptionLabel.frame.maxY + 5)
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }
    }
    
    override public func layoutSubviews() {
        print("layoutSubviews with state : +++\(componentState)+++")
        print("layoutSubviews with state : frame : \(self.frame)+++")
        let width: CGFloat = self.frame.size.width
        if( width != lastWidth ){
            lastWidth = width
            delay(0.0){
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
            }
        }
        let lineHeight: CGFloat = componentState == .empty ? 1 : 2
        
        var leftImageFrame: CGRect = CGRect.zero
        var rightImageFrame: CGRect = CGRect.zero
        
        if(leftImageName != nil){
            if(paddingLeft == 16){ paddingLeft = 10 }
            leftImageFrame = CGRect(x: 0, y: 0, width: 36, height: 36)
            leftImageFrame = CGRect(x: paddingLeft, y: 0, width: 36, height: 36)
        }
        if(rightImageName != nil) { rightImageFrame = CGRect(x: 0, y: 0, width: 24, height: 24) }
        
        let usableWidth: CGFloat = leftImageFrame.width == 0 ? width-paddingLeft-paddingRight : width-62-paddingLeft-paddingRight
        
        let titleSize = self.titleLabel.sizeThatFits( CGSize(width: usableWidth, height:10000) )
        let textFieldSize = CGSize(width: usableWidth, height: inputTextField.sizeThatFits( CGSize(width:10000, height:10000)).height )
        let invalidDescriptionSize = self.invalidDescriptionLabel.sizeThatFits( CGSize(width: usableWidth, height:10000) )
        
        let leftSpace: CGFloat = leftImageFrame.width == 0 ? paddingLeft : 62 + paddingLeft
        
        /*
         .empty => 1
         .emptyInvalid => 2
         .editing => 4
         .editingInvalid => 3
         .filled => 4
         .filledInvalid => 3
         */
        
        switch componentState {
        case .emptyDisabled, .empty:
            titleLabel.frame = CGRect(x: leftSpace, y: 26, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: 26, width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y: max( inputTextField.frame.maxY+5, titleLabel.frame.maxY+5 ) , width: usableWidth, height: lineHeight)
            inputTextField.frame = CGRect(x: leftSpace, y: lineView.frame.origin.y-5-textFieldSize.height, width: textFieldSize.width, height: textFieldSize.height)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y:lineView.frame.maxY+3, width: usableWidth, height: invalidDescriptionSize.height)
        case .editing, .filled, .filledDisabled:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: max(26, titleLabel.frame.maxY+2), width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y:inputTextField.frame.maxY+5, width: usableWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y:lineView.frame.maxY+3, width: usableWidth, height: invalidDescriptionSize.height)
        case .emptyInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 26, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: 26, width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y: max( inputTextField.frame.maxY+5, titleLabel.frame.maxY+5 ) , width: usableWidth, height: lineHeight)
            inputTextField.frame = CGRect(x: leftSpace, y: lineView.frame.origin.y-5-textFieldSize.height, width: textFieldSize.width, height: textFieldSize.height)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
        case .editingInvalid, .filledInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: max(26, titleLabel.frame.maxY+2), width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y:inputTextField.frame.maxY+5, width: usableWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
        
        default:
            fatalError("unrecognized state on layoutSubviews")
        }
        
        leftImageFrame.origin.y = lineView.frame.origin.y - leftImageFrame.size.height
        leftImageFrame.origin.y = leftImageFrame.origin.y + leftImageCenterYOffset
        leftImageFrame.origin.x = leftImageFrame.origin.x + leftImageCenterXOffset
        
        rightImageFrame.origin.x = width-paddingRight-rightImageFrame.size.width
        rightImageFrame.origin.y = lineView.frame.origin.y - rightImageFrame.size.height - 7
        rightImageFrame.origin.y = rightImageFrame.origin.y + rightImageCenterYOffset
        rightImageFrame.origin.x = rightImageFrame.origin.x + rightImageCenterXOffset
        
        leftImageView.frame = leftImageFrame
        rightImageView.frame = rightImageFrame
    }
    
    override public var debugDescription: String {
        get {
            return "component state : \(self.componentState) \(super.debugDescription)"
        }
    }
    
    deinit {
        // remove all targets and notifications
    }
}
