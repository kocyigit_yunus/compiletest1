//
//  TebTextFieldType1.swift
//  Cepteteb
//
//  Created by Yunus Koçyiğit on 16/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation
import UIKit

fileprivate var EMPTY_SPACE = "  "
fileprivate var DEFAULT_PLACEHOLDER = "0,00"
fileprivate var INTEGER_PART_MAX = 7
fileprivate var DECIMAL_PART_MAX = 2
fileprivate var COMMA = ","
fileprivate var DOT = "."

public class TebAmountTextField: TebBaseComponent, UITextFieldDelegate {
    
    // TebAmountTextField
    
    public var inputTextField: UITextField!
    public var titleLabel: UILabel!
    public var lineView: UIView!
    public var invalidDescriptionLabel: UILabel!
    private var lastWidth: CGFloat = -10000
    public weak var tebTextFieldDelegate: TebTextFieldDelegate?
    
    // MARK: Validatable
    
    override public var errorMessage: String? {
        didSet {
            self.invalidDescriptionLabel.text = errorMessage
        }
    }
    
    override public func validate() {
        isValid = true
        self.errorMessage = nil
        self.validateState = .onChange
        
        if(checkValidation != true){
            return
        }
        
        for validator in validators!.sorted( by: { $0.priority > $1.priority }) {
            
            var potentialErrorMessage: String? = nil
            
            if( validator is MinValueValidator || validator is MaxValueValidator ){
                potentialErrorMessage = validator.validate(value: self.doubleValue as AnyObject?)
            }else {
                potentialErrorMessage = validator.validate(value: self.inputTextField.text as AnyObject?)
            }
            
            if( potentialErrorMessage != nil ) {
                isValid = false
                self.errorMessage = potentialErrorMessage
                return
            }
            
        }
        
    }
    
    override public func updateComponentStateForValidation() {
        if( self.isValid == false ) {
            if(inputTextField.text != nil &&  inputTextField.text!.characters.count > 0){
                self.updateComponentState(newState: .filledInvalid, animated: true)
            }else{
                self.updateComponentState(newState: .filledInvalid, animated: true)
            }
        }else{
            if(inputTextField.text != nil &&  inputTextField.text!.characters.count > 0){
                self.updateComponentState(newState: .filled, animated: true)
            }else{
                self.updateComponentState(newState: .filled, animated: true)
            }
        }
    }
    
    // MARK: Text Field Delegate
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        cursorPosition = (count - 3)
        if( self.componentState == .emptyInvalid || self.componentState == .editingInvalid ){
            self.updateComponentState(newState: .editingInvalid, animated: true)
            tebTextFieldDelegate?.tebTextField?(editingDidBegin: self.inputTextField)
            return
        }
        self.updateComponentState(newState: .editing, animated: true)
        tebTextFieldDelegate?.tebTextField?(editingDidBegin: self.inputTextField)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.validateState = .onChange
        self.resignFirstResponder()
        self.validate()
        updateComponentStateForValidation()
        tebTextFieldDelegate?.tebTextField?(editingDidEnd: self.inputTextField)
    }
    
    func textFieldDidChange(sender: UITextField?){
        if( self.validateState == .onChange ){
            self.validate()
            if( self.isValid ){
                // valid
                if( self.componentState != .editing ){
                    self.updateComponentState(newState: .editing, animated: true)
                }
            }else{
                // invalid
                if( self.componentState != .editingInvalid ){
                    self.updateComponentState(newState: .editingInvalid, animated: true)
                }
            }
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Construct the text that will be in the field if this change is accepted
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            insertText(string: string, range: range)
        case COMMA:
            cursorPosition = (count - 2)
        default:
            // Assume this is delete key
            delete()
        }
        editingChanged()
        return false
        
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    
    // MARK: Amount
    
    public var paraKod: String = "TL"{
        didSet{
            self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)"
        }
    }
    
    fileprivate lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.decimalSeparator = COMMA
        formatter.groupingSeparator = DOT
        formatter.minimumFractionDigits = DECIMAL_PART_MAX
        formatter.maximumFractionDigits = DECIMAL_PART_MAX
        formatter.numberStyle = .decimal
        return formatter
    }()
    
    func currentStringWithoutParakod() -> String? {
        return self.inputTextField.text?.replacingOccurrences(of: self.paraKod, with: "").replacingOccurrences(of: " ", with: "")
    }
    
    var currentString: String {
        get {
            return inputTextField.text ?? "\(DEFAULT_PLACEHOLDER) \(paraKod)"
        }
        set {
            let cleanNewValue = newValue.replacingOccurrences(of: DOT, with: "").replacingOccurrences(of: paraKod, with: "").trimmingCharacters(in: CharacterSet.whitespaces)
            guard
                let doubleNewValue = formatter.number(from: cleanNewValue)
                else {
                    self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)"
                    return
            }
            if doubleNewValue == 0 {
                self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)"
            } else {
                guard let stringValue = formatter.string(from: doubleNewValue) else {
                    self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)"
                    return
                }
                self.inputTextField.text = "\(stringValue) \(paraKod)"
            }
        }
    }
    
    fileprivate var count: Int {
        return inputTextField.text!.characters.count - paraKod.characters.count - 1
    }
    
    open var doubleValue: Double {
        get {
            let cleanValue = inputTextField.text!.replacingOccurrences(of: DOT, with: "").replacingOccurrences(of: paraKod, with: "").trimmingCharacters(in: CharacterSet.whitespaces)
            return formatter.number(from: cleanValue)?.doubleValue ?? 0.0
        }
        set {
            guard let s = formatter.string(from: NSNumber(value: newValue)) else {
                self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)"
                return
            }
            inputTextField.text = "\(s) \(paraKod)"
        }
    }
    
    fileprivate func insertText(string: String, range: NSRange) {
        
        if count == 4 && currentString.charAt(index: 0) == "0" && cursorPosition == 1 {
            currentString.replace(start: 0, end: 1, string: string)
            cursorPosition = count - 3
        } else if cursorPosition == (count - 3) {
            currentString.insert(at: (count - 3), string: string)
            cursorPosition = count - 3
        } else if cursorPosition == (count - 2) {
            currentString.replace(start: (count - 2), end: (count - 1), string: string)
            cursorPosition = count - 1
        } else if cursorPosition == (count - 1) {
            currentString.replace(start: (count - 1), end: (count), string: string)
            cursorPosition = count
        } else if cursorPosition == count {
            // end of the text, can't do anything
        } else {
            cursorPosition = (count - 3)
        }
        
    }
    
    fileprivate func delete() {
        
        if count == cursorPosition { // Cursor at the end, set last char to 0, move cursor to the left
            currentString.replace(start: count - 1, end: count, string: "0")
            cursorPosition = (count - 1)
        } else if (count - 1) == cursorPosition { // cursor at the left of last char
            currentString.replace(start: (count - 2), end: (count - 1), string: "0")
            cursorPosition = (count - 2)
        } else if (count - 2) == cursorPosition { // cursor at right of coma, move it left
            cursorPosition = (count - 3)
        } else {
            if count == 4 {
                currentString.replace(start: (count - 4), end: (count - 3), string: "0")
            } else if (count - 3) == cursorPosition {
                currentString.replace(start: (count - 4), end: (count - 3), string: "")
            }
            cursorPosition = (count - 3)
        }
        
    }
    
    fileprivate var cursorPosition: Int? {
        get {
            if let selectedRange = inputTextField.selectedTextRange {
                return inputTextField.offset(from: inputTextField.beginningOfDocument, to: selectedRange.start)
            }
            return nil
        }
        set {
            // only if there is a currently selected range
            if let selectedRange = inputTextField.selectedTextRange, let offset = newValue {
                if let newPosition = inputTextField.position(from: inputTextField.beginningOfDocument, offset: offset) {
                    inputTextField.selectedTextRange = inputTextField.textRange(from: newPosition, to: newPosition)
                }
            }
        }
    }
    
    
    
    // Invoked when the editing state of the textfield changes. Override to respond to this change.
    open func editingChanged() {
        
        tebTextFieldDelegate?.tebTextField?(editingChanged: self.inputTextField)
        
        /*
         if text == nil || text!.isEmpty {
         ceptetebTextFieldState = .active
         } else {
         self.ceptetebTextFieldState = .editing
         }
         
         if(isUppercased == true){
         self.text = self.text?.uppercased()
         }
         
         validate()
         ceptetebTextFieldDelegate?.ceptetebTextField?(editingChanged: self)
         */
    }
    
    // MARK: Did Load And Update Properties
    
    override public func didLoad(){
        super.didLoad()
        self.setContentCompressionResistancePriority(1000, for: .vertical)
        
        titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.font = UIFont.brandonRegular16
        titleLabel.textColor = colorEmpty
        titleLabel.text = titleTextEmpty
        titleLabel.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ titleLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(titleLabel)
        
        lineView = UIView(frame: CGRect.zero)
        lineView.backgroundColor = colorEmpty
        self.addSubview(lineView)
        
        inputTextField = UITextField(frame: CGRect.zero)
        inputTextField.font = UIFont.brandonRegular16
        inputTextField.textColor = UIColor.ceptetebDark80Color()
        inputTextField.delegate = self
        inputTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        if(DevelopmentHelper.markComponentLevel2){ inputTextField.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(inputTextField)
        
        invalidDescriptionLabel = UILabel(frame: CGRect.zero)
        invalidDescriptionLabel.font = UIFont.brandonRegular14
        invalidDescriptionLabel.textColor = self.colorInvalid
        invalidDescriptionLabel.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ invalidDescriptionLabel.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(invalidDescriptionLabel)
        
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
        
        self.inputTextField.keyboardType = .decimalPad // Set keyboard type
        self.inputTextField.text = "\(DEFAULT_PLACEHOLDER) \(paraKod)" // Set default value
        
        updateComponentState(newState: .filled, animated: false)
        //updateProperties(animated: false)
    }
    
    override public func updateProperties(animated: Bool){
        super.updateProperties(animated: animated)
        
        if(self.componentState == .emptyDisabled ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextDisabled
            self.titleLabel.textColor = self.colorDisabled
            self.lineView.backgroundColor = self.colorDisabled
        }else if( self.componentState == .empty ){
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorEmpty
            self.lineView.backgroundColor = self.colorEmpty
        }else if( self.componentState == .emptyInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular16
            self.titleLabel.text = self.titleTextEmpty
            self.titleLabel.textColor = self.colorEmpty
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .editing ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextEditing
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorFiled
        }else if( self.componentState == .editingInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .filledInvalid ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextInvalid
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorInvalid
        }else if( self.componentState == .filled ) {
            self.titleLabel.font = UIFont.brandonRegular14
            self.titleLabel.text = self.titleTextFilled
            self.titleLabel.textColor = UIColor.ceptetebDark40Color()
            self.lineView.backgroundColor = self.colorFiled
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }
    }
    
    // MARK: Layout
    
    override open var intrinsicContentSize : CGSize {
        print("intrinsicContentSize with state : +++\(componentState)+++")
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        let targetedSize: CGSize = CGSize(width: self.frame.size.width, height: 10000)
        if( componentState == .empty || componentState == .emptyDisabled || componentState == .filled || componentState == .editing ){
            let calculatedSize = CGSize(width: targetedSize.width, height: lineView.frame.maxY + 16 )
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }
        else if( componentState == .emptyInvalid || componentState == .filledInvalid || componentState == .editingInvalid ){
            let calculatedSize = CGSize(width: targetedSize.width, height: invalidDescriptionLabel.frame.maxY + 5)
            print("calculated size : \(calculatedSize)")
            return calculatedSize
        }else{
            fatalError("unrecognized state on systemLayoutSizeFitting")
        }
    }
    
    override public func layoutSubviews() {
        print("layoutSubviews with state : +++\(componentState)+++")
        print("layoutSubviews with state : frame : \(self.frame)+++")
        let width: CGFloat = self.frame.size.width
        if( width != lastWidth ){
            lastWidth = width
            delay(0.0){
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
            }
        }
        let lineHeight: CGFloat = componentState == .empty ? 1 : 2
        
        var leftImageFrame: CGRect = CGRect.zero
        var rightImageFrame: CGRect = CGRect.zero
        
        if(leftImageName != nil){
            if(paddingLeft == 16){ paddingLeft = 10 }
            leftImageFrame = CGRect(x: 0, y: 0, width: 36, height: 36)
            leftImageFrame = CGRect(x: paddingLeft, y: 0, width: 36, height: 36)
        }
        if(rightImageName != nil) { rightImageFrame = CGRect(x: 0, y: 0, width: 24, height: 24) }
        
        let usableWidth: CGFloat = leftImageFrame.width == 0 ? width-paddingLeft-paddingRight : width-62-paddingLeft-paddingRight
        
        let titleSize = self.titleLabel.sizeThatFits( CGSize(width: usableWidth, height:10000) )
        let textFieldSize = CGSize(width: usableWidth, height: inputTextField.sizeThatFits( CGSize(width:10000, height:10000)).height )
        let invalidDescriptionSize = self.invalidDescriptionLabel.sizeThatFits( CGSize(width: usableWidth, height:10000) )
        
        let leftSpace: CGFloat = leftImageFrame.width == 0 ? paddingLeft : 62 + paddingLeft
        
        /*
         .empty => 1
         .emptyInvalid => 2
         .editing => 4
         .editingInvalid => 3
         .filled => 4
         .filledInvalid => 3
         */
        
        switch componentState {
        case.emptyDisabled, .empty, .filled, .editing:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: max(26, titleLabel.frame.maxY+2), width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y:inputTextField.frame.maxY+5, width: usableWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y:lineView.frame.maxY+3, width: usableWidth, height: 0)
        case .emptyInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 26, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: 26, width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y: max( inputTextField.frame.maxY+5, titleLabel.frame.maxY+5 ) , width: usableWidth, height: lineHeight)
            inputTextField.frame = CGRect(x: leftSpace, y: lineView.frame.origin.y-5-textFieldSize.height, width: textFieldSize.width, height: textFieldSize.height)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
        case .editingInvalid, .filledInvalid:
            titleLabel.frame = CGRect(x: leftSpace, y: 5, width: titleSize.width, height: titleSize.height)
            inputTextField.frame = CGRect(x: leftSpace, y: max(26, titleLabel.frame.maxY+2), width: textFieldSize.width, height: textFieldSize.height)
            lineView.frame = CGRect(x: leftSpace, y:inputTextField.frame.maxY+5, width: usableWidth, height: lineHeight)
            invalidDescriptionLabel.frame = CGRect(x: leftSpace, y: lineView.frame.maxY+3, width: invalidDescriptionSize.width, height: invalidDescriptionSize.height)
            
        default:
            fatalError("unrecognized state on layoutSubviews")
        }
        
        leftImageFrame.origin.y = lineView.frame.origin.y - leftImageFrame.size.height
        leftImageFrame.origin.y = leftImageFrame.origin.y + leftImageCenterYOffset
        leftImageFrame.origin.x = leftImageFrame.origin.x + leftImageCenterXOffset
        
        rightImageFrame.origin.x = width-paddingRight-rightImageFrame.size.width
        rightImageFrame.origin.y = lineView.frame.origin.y - rightImageFrame.size.height - 7
        rightImageFrame.origin.y = rightImageFrame.origin.y + rightImageCenterYOffset
        rightImageFrame.origin.x = rightImageFrame.origin.x + rightImageCenterXOffset
        
        leftImageView.frame = leftImageFrame
        rightImageView.frame = rightImageFrame
    }
    
    override public var debugDescription: String {
        get {
            return "component state : \(self.componentState) \(super.debugDescription)"
        }
    }
    
    deinit {
        // remove all targets and notifications
    }
    
}

fileprivate extension UITextField {
    // Moves the caret to the correct position by removing the trailing whitespace
    func fixCaretPosition() {
        // Moving the caret to the correct position by removing the trailing whitespace
        // http://stackoverflow.com/questions/14220187/uitextfield-has-trailing-whitespace-after-securetextentry-toggle
        
        let beginning = self.beginningOfDocument
        self.selectedTextRange = self.textRange(from: beginning, to: beginning)
        let end = self.endOfDocument
        self.selectedTextRange = self.textRange(from: end, to: end)
    }
}


fileprivate extension String {
    
    func charAt(index: Int) -> String? {
        if self.characters.count > index {
            return self.substring(to: self.index(self.startIndex, offsetBy: (index + 1)))
        }
        return nil
    }
    
    mutating func replace(start: Int, end: Int, string: String) {
        let startIndex = self.index(self.startIndex, offsetBy: start)
        let endIndex = self.index(self.startIndex, offsetBy: end)
        let range = Range<String.Index>.init(uncheckedBounds: (lower: startIndex, upper: endIndex))
        self = self.replacingCharacters(in: range, with: string)
    }
    
    mutating func insert(at: Int, string: String) {
        var index = self.index(self.startIndex, offsetBy: at)
        self.insert(contentsOf: string.characters, at: index)
    }
    
    func substring(from: Int, to: Int) -> String {
        return ""
    }
}

