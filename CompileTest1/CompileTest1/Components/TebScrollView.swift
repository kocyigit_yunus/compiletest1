//
//  TebScrollView.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 07/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit
import TPKeyboardAvoiding

class TebScrollView: TPKeyboardAvoidingScrollView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        didLoad()
    }
    
    func didLoad(){
        keyboardDismissMode = .onDrag
    }
    
    public func isValid() -> Bool {
        
        self.validateSubviews()
        let invalidSubview = self.deepestFirstInvalidSubview()
        
        if( invalidSubview != nil ){
            
            UIView.animate(withDuration: 0.3) {
                self.updateSubviewValidationStates()
                self.layoutIfNeeded()
                let yPositionOfInvalidSubview = self.findYPositionForView(targetView: invalidSubview!)
                self.setContentOffset(CGPoint(x: 0, y: yPositionOfInvalidSubview), animated: false)
            }
            
        }
        
        return invalidSubview != nil ? false : true
        
    }
    
    override func tpKeyboardAvoiding_findFirstResponderBeneathView(_ view: UIView!) -> UIView! {
        let willReturn: UIView? = super.tpKeyboardAvoiding_findFirstResponderBeneathView(view)
        if( willReturn?.superview != nil && willReturn?.superview is TebBaseComponent ){
            return willReturn!.superview!
        }else{
            return willReturn
        }
    }
    
    override func tpKeyboardAvoiding_scrollToActiveTextField() {
        delay(0.00){
            //super.tpKeyboardAvoiding_scrollToActiveTextField()
        }
        super.tpKeyboardAvoiding_scrollToActiveTextField()
    }
    
    override func touchesShouldCancel(in view: UIView) -> Bool {
        return true
    }
    
    public func findYPositionForView(targetView : UIView) -> CGFloat {
        
        var closestSubview = self.findClosestParentOfView(targetView: targetView)
        if( closestSubview is UIStackView || closestSubview != nil /* content view uiview yada stack view olabilir */ ){
            closestSubview = closestSubview?.findClosestParentOfView(targetView: targetView)
            
            // scroll view in frame inin height i
            let scrollHeight: CGFloat = self.frame.size.height
            
            // scrollanmak istenen alanın y koordinatı
            let subviewY: CGFloat = closestSubview!.frame.origin.y
            
            // scrol lview in contentinin height i ve content insetleri
            let contentHeight: CGFloat = self.contentSize.height
            let contentInsetTop: CGFloat = self.contentInset.top
            let contentInsetBottom: CGFloat = self.contentInset.bottom
            
            // y ekseninde scrollanılabilecek maksimum deger
            let scrollableMax = max(0,contentHeight-scrollHeight+contentInsetTop+contentInsetBottom)
            
            // y ekseninde scrollanılacak değer yukarıdan ne kadar altta olmalı
            let marginForScroll: CGFloat = 0
            
            // scrollanılacak değerin content inset top hesaplanmadan önceki hali
            let scrollTo = min(scrollableMax, subviewY-marginForScroll)
            
            // scrollanılacak gerçek değer :)
            let willReturn = scrollTo-contentInsetTop
            
            print("will return : \(willReturn)")
            
            return willReturn
        }
        
        return -1
        
    }
    
    override public var debugDescription: String {
        get {
            return "content inset : \(self.contentInset) \(super.debugDescription)"
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
