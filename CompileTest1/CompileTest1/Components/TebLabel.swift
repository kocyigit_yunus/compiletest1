//
//  TebLabel.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 08/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class TebLabel: UILabel {
    
    var defaultKern: CGFloat = 0.25
    
    override var text: String? {
        didSet {
            if(text != nil){
                var attributedStringAttributes = [String : Any]()
                attributedStringAttributes[NSFontAttributeName] = self.font
                attributedStringAttributes[NSForegroundColorAttributeName] = self.textColor
                attributedStringAttributes[NSKernAttributeName] = defaultKern
                let attributedString = NSAttributedString(string: self.text!, attributes: attributedStringAttributes)
                self.attributedText = attributedString
            }else{
                self.attributedText = NSAttributedString()
            }
        }
    }
    
}
