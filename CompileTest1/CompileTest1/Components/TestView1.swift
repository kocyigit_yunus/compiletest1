//
//  TestView1.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class TestView1: UIView {
    
    var lbl1: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        didLoad()
    }
    
    func didLoad(){
        self.lbl1 = UILabel(frame: CGRect.zero)
        self.lbl1.text = "Deneme123"
        self.lbl1.backgroundColor = UIColor.red
        self.addSubview(self.lbl1)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width, height: 100)
    }
    
    override func layoutSubviews() {
        let width: CGFloat = self.frame.size.width
        let height: CGFloat = self.frame.size.height
        
        let lbl1Size = self.lbl1.sizeThatFits( CGSize(width: width, height:CGFloat.greatestFiniteMagnitude) )
        
        self.lbl1.sizeToFit()
        self.lbl1.center = CGPoint(x: width/2, y: height/2)
        
    }
}
