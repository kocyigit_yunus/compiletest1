//
//  PickerType2.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 31/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

enum TebPickerType1State: Int {
    case emptyDisabled = 1
    case empty = 2
    case selected = 3
    case invalid = 4
}

class TebPickerType1: UIControl, Validatable, UITextFieldDelegate {
    
    // Validatable
    var isValid: Bool = true
    var errorMessage: String? = nil
    var checkValidation: Bool = true
    var validators: [Validator]? = [Validator]()
    func validate() {
        isValid = true
        errorMessage = nil
        
        if(checkValidation != true){
            return
        }
        
        for validator in validators!.sorted( by: { $0.priority > $1.priority }) {
            
            var validationResult: String? = nil
            if( validator is RequiredValidator ){
                let value:String? =  self.pickerState == .selected ? "" : nil
                validationResult = validator.validate(value: value as AnyObject? )
            }else{
                validationResult = nil
            }
            
            if(validationResult != nil){
                errorMessage = validationResult
                invalidDescriptionText = errorMessage
                isValid = false
                return
            }
        }
        
    }
    
    func updateComponentStateForValidation() {
        if(isValid == false){
            self.updateState(state: .invalid, animated: false)
        }
    }
    
    // UI Elements
    private var lblPlaceholder: TebLabel!
    private var lblTitle: UILabel!
    private var imgRight: UIImageView!
    private var imgLeft: UIImageView!
    private var vwLineBottom: UIView!
    private var vwImageLeftHolder: UIView!
    private var lblInvalidDescription : UILabel!
    
    var preserveInvalidStateSpace: Bool = true
    
    var paddingLeft: CGFloat = 10 // 10
    var paddingRight: CGFloat = 16 // 16
    var paddingTop: CGFloat = 11 // 11
    var paddingBottom: CGFloat = 8 // 8
    
    var pickerState: TebPickerType1State = .empty
    var imgRightXOffset: CGFloat = 5
    var imgRightYOffset: CGFloat = 0
    var imgLeftXOffset: CGFloat = 0
    var imgLeftYOffset: CGFloat = 0
    
    var bottomLineColorDisabled: UIColor = UIColor.ceptetebDark40Color()
    var bottomLineColorEmpty: UIColor = UIColor.ceptetebDark40Color()
    var bottomLineColorSelected: UIColor = UIColor.ceptetebShamrockGreenColor()
    var bottomLineColorInvalid: UIColor = UIColor.ceptetebBrightOrangeColor()
    
    var imageLeftDisabled: String? = "wallet_green"
    var imageLeftEmpty: String? = "wallet_green"
    var imageLeftSelected: String? = "wallet_green"
    var imageLeftInvalid: String? = "wallet_green"
    
    var imageRightDisabled: String? = "arrow_right_green"
    var imageRightEmpty: String? = "arrow_right_green"
    var imageRightSelected: String? = "arrow_right_green"
    var imageRightInvalid: String? = "arrow_right_green"
    
    var placeholderTextDisabled: String! = "Alıcı Banka Seçilemiyor"
    var placeholderTextEmpty: String! = "Alıcı Banka Seçiniz"
    var placeholderTextSelected: String! = "Alıcı Banka"
    var placeholderTextInvalid: String! = "Alıcı Banka Seçiniz"
    
    var text: String! = "TEB" { didSet { lblTitle.text = text } }
    var invalidDescriptionText: String! = "Alıcı Banka Seçimi Zorunludur!" { didSet { lblInvalidDescription.text = invalidDescriptionText } }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func didLoad(){
        lblPlaceholder = TebLabel(frame: CGRect.zero)
        lblPlaceholder.text = placeholderTextEmpty
        lblPlaceholder.font = UIFont.brandonRegular14
        if(DevelopmentHelper.markComponentLevel2){ lblPlaceholder.backgroundColor = UIColor.markColorBg2() }
        lblPlaceholder.textColor = UIColor.ceptetebDark40Color()
        self.addSubview(lblPlaceholder)
        
        lblTitle = UILabel(frame: CGRect.zero)
        lblTitle.text = text
        lblTitle.font = UIFont.brandonRegular16
        lblTitle.textColor = UIColor.ceptetebDark80Color()
        if(DevelopmentHelper.markComponentLevel2){ lblTitle.backgroundColor = UIColor.markColorBg2() }
        lblTitle.textColor = UIColor.ceptetebDarkGreyBlueColor()
        self.addSubview(lblTitle)
        
        vwLineBottom = UIView(frame: CGRect.zero)
        vwLineBottom.backgroundColor = UIColor.ceptetebDark40Color()
        self.addSubview(vwLineBottom)
        
        vwImageLeftHolder = UIView(frame: CGRect.zero)
        if(DevelopmentHelper.markComponentLevel2){ vwImageLeftHolder.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(vwImageLeftHolder)
        
        imgLeft = UIImageView(frame: CGRect.zero)
        if(DevelopmentHelper.markComponentLevel3){ imgLeft.backgroundColor = UIColor.markColorBg3() }
        vwImageLeftHolder.addSubview(imgLeft)
        
        imgRight = UIImageView(frame: CGRect.zero)
        if(DevelopmentHelper.markComponentLevel2){ imgRight.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(imgRight)
        
        lblInvalidDescription = UILabel(frame: CGRect.zero)
        lblInvalidDescription.text = invalidDescriptionText
        lblInvalidDescription.font = UIFont.brandonRegular14
        lblInvalidDescription.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ lblInvalidDescription.backgroundColor = UIColor.markColorBg2() }
        lblInvalidDescription.textColor = UIColor.ceptetebBrightOrangeColor()
        self.addSubview(lblInvalidDescription)
        
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
        updatePickerState()
    }
    
    func updateState(state: TebPickerType1State, animated: Bool){
        if(state != pickerState){
            pickerState = state
        }
        if(self.superview != nil ){
            if(animated){
                UIView.animate(withDuration: 0.4, animations: {
                    self.updatePickerState()
                    self.superview?.setNeedsLayout()
                    self.superview?.layoutIfNeeded()
                })
            }else{
                self.updatePickerState()
                self.superview?.setNeedsLayout()
                self.superview?.layoutIfNeeded()
            }
        }else {
            if(animated){
                UIView.animate(withDuration: 0.4, animations: {
                    self.updatePickerState()
                })
            }else{
                updatePickerState()
            }
        }
    }
    
    func updatePlaceholderText() {
        if(pickerState == .emptyDisabled){
            lblPlaceholder.text = placeholderTextDisabled
        }else if(pickerState == .empty){
            lblPlaceholder.text = placeholderTextEmpty
        }else if(pickerState == .selected){
            lblPlaceholder.text = placeholderTextSelected
        }else if(pickerState == .invalid){
            lblPlaceholder.text = placeholderTextInvalid
        }
    }
    
    func updateRightImage(){
        if(pickerState == .emptyDisabled){
            imgRight.image = imageRightDisabled != nil ? UIImage(named:imageRightDisabled!) : nil
        }else if(pickerState == .empty){
            imgRight.image = imageRightEmpty != nil ? UIImage(named:imageRightEmpty!) : nil
        }else if(pickerState == .selected){
            imgRight.image = imageRightSelected != nil ? UIImage(named:imageRightSelected!) : nil
        }else if(pickerState == .invalid){
            imgRight.image = imageRightInvalid != nil ? UIImage(named:imageRightInvalid!) : nil
        }
        imgRight.layer.add(CATransition(), forKey: kCATransition)
    }
    
    func updateLeftImage(){
        if(pickerState == .emptyDisabled){
            imgLeft.image = imageLeftDisabled != nil ? UIImage(named:imageLeftDisabled!) : nil
        }else if(pickerState == .empty){
            imgLeft.image = imageLeftEmpty != nil ? UIImage(named:imageLeftEmpty!) : nil
        }else if(pickerState == .selected){
            imgLeft.image = imageLeftSelected != nil ? UIImage(named:imageLeftSelected!) : nil
        }else if(pickerState == .invalid){
            imgLeft.image = imageLeftInvalid != nil ? UIImage(named:imageLeftInvalid!) : nil
        }
        imgLeft.layer.add(CATransition(), forKey: kCATransition)
    }
    
    func updateProperties(){
        if(pickerState == .emptyDisabled){
            lblTitle.alpha = 0
            lblPlaceholder.font = UIFont.brandonRegular16
            lblPlaceholder.text = placeholderTextDisabled
            vwLineBottom.backgroundColor = bottomLineColorDisabled
            lblInvalidDescription.alpha = 0
        }else if(pickerState == .empty){
            lblTitle.alpha = 0
            lblPlaceholder.font = UIFont.brandonRegular16
            lblPlaceholder.text = placeholderTextEmpty
            vwLineBottom.backgroundColor = bottomLineColorEmpty
            lblInvalidDescription.alpha = 0
        }else if(pickerState == .selected){
            lblTitle.alpha = 1
            lblPlaceholder.font = UIFont.brandonRegular14
            lblPlaceholder.text = placeholderTextSelected
            vwLineBottom.backgroundColor = bottomLineColorSelected
            lblInvalidDescription.alpha = 0
        }else if(pickerState == .invalid){
            lblInvalidDescription.alpha = 1
            vwLineBottom.backgroundColor = bottomLineColorInvalid
            lblTitle.alpha = 0
            lblPlaceholder.font = UIFont.brandonRegular16
            lblPlaceholder.text = placeholderTextInvalid
        }
    }
    
    func updatePickerState() {
        updateLeftImage()
        updateRightImage()
        remakeConstraints()
        updateProperties()
    }
    
    func remakeConstraints(){
        if(pickerState == .emptyDisabled){
            emptyConstraints()
        }else if(pickerState == .empty){
            emptyConstraints()
        }else if(pickerState == .selected){
            selectedConstraints()
        }else if(pickerState == .invalid){
            invalidConstrainsts()
        }
    }
    
    func emptyConstraints() {
        
        clearContraints()
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.right.lessThanOrEqualToSuperview().offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(68)
            make.right.equalTo(-paddingRight)
            make.left.equalTo(vwImageLeftHolder.snp.left).offset(54)
            make.height.equalTo(1)
            if( preserveInvalidStateSpace ){
                make.bottom.equalToSuperview().offset(-36.5)
            }else {
                make.bottom.equalToSuperview().offset(-8)
            }
        }
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop).priority(100)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(vwLineBottom.snp.left)
            make.centerY.equalTo(lblPlaceholder.snp.centerY)
            make.width.lessThanOrEqualTo(lblPlaceholder)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
        vwImageLeftHolder.snp.makeConstraints { (make) in
            make.left.equalTo(paddingLeft)
            make.bottom.equalTo(vwLineBottom.snp.bottom).offset(-1)
            make.width.equalTo(36)
            make.height.equalTo(36)
        }
        
        imgLeft.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(imgLeftXOffset)
            make.centerY.equalToSuperview().offset(imgLeftYOffset)
        }
        
    }
    
    func selectedConstraints() {
        
        clearContraints()
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.right.lessThanOrEqualToSuperview().offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalToSuperview().offset(-paddingBottom)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(68)
            make.right.equalTo(-paddingRight)
            make.left.equalTo(vwImageLeftHolder.snp.left).offset(54)
            make.height.equalTo(1)
            if( preserveInvalidStateSpace ){
                make.bottom.equalToSuperview().offset(-36.5)
            }else {
                make.bottom.equalToSuperview().offset(-8)
            }
        }
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop)
            make.bottom.equalTo(lblTitle.snp.top).offset(-4).priority(900)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
        vwImageLeftHolder.snp.makeConstraints { (make) in
            make.left.equalTo(paddingLeft)
            make.bottom.equalTo(vwLineBottom.snp.bottom).offset(-1)
            make.width.equalTo(36)
            make.height.equalTo(36)
        }
        
        imgLeft.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(imgLeftXOffset)
            make.centerY.equalToSuperview().offset(imgLeftYOffset)
        }
        
    }
    
    func invalidConstrainsts(){
        
        clearContraints()
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop).priority(100)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(vwLineBottom.snp.left)
            make.centerY.equalTo(lblPlaceholder.snp.centerY)
            make.width.lessThanOrEqualTo(lblPlaceholder)
        }
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.right.lessThanOrEqualToSuperview().offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalToSuperview().offset(-paddingBottom)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(68)
            make.right.equalTo(-paddingRight)
            make.left.equalTo(vwImageLeftHolder.snp.left).offset(54)
            make.height.equalTo(1)
            make.bottom.equalToSuperview().offset(-36.5)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
        vwImageLeftHolder.snp.makeConstraints { (make) in
            make.left.equalTo(paddingLeft)
            make.bottom.equalTo(vwLineBottom.snp.bottom).offset(-1)
            make.width.equalTo(36)
            make.height.equalTo(36)
        }
        
        imgLeft.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(imgLeftXOffset)
            make.centerY.equalToSuperview().offset(imgLeftYOffset)
        }
        
    }
    
    func clearContraints(){
        lblInvalidDescription.snp.removeConstraints()
        vwLineBottom.snp.removeConstraints()
        lblPlaceholder.snp.removeConstraints()
        lblTitle.snp.removeConstraints()
        imgRight.snp.removeConstraints()
        vwImageLeftHolder.snp.removeConstraints()
        imgLeft.snp.removeConstraints()
    }
    
    deinit {
        //self.removeTarget(nil, action: nil, for: .allEvents)
    }
}
