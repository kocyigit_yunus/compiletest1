//
//  TebFormInfoView.swift
//  CompileTest1
//
//  Created by Yunus Koçyiğit on 20/01/2017.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

public class TebFormInfoView: UIView {
    
    public var infoLabel: UILabel!
    
    public var topPadding: CGFloat = 8
    public var leftPadding: CGFloat = 16
    public var rightPadding: CGFloat = 16
    public var bottomPadding: CGFloat = 8
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func didLoad(){
        self.setContentCompressionResistancePriority(1000, for: .vertical)
        
        infoLabel = UILabel()
        infoLabel.font = UIFont.brandonRegular14
        infoLabel.textColor = UIColor.ceptetebDark80Color()
        infoLabel.numberOfLines = 0
        self.addSubview(self.infoLabel)
        
        if(DevelopmentHelper.markComponentLevel2){ infoLabel.backgroundColor = UIColor.markColorBg2() }
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
    }
    
    public var text: String? {
        get {
            return self.infoLabel.text
        }
        set {
            self.updateText(text: newValue, animated: false)
        }
    }
    
    public func updateText(text: String?, animated: Bool){
        self.infoLabel.text = text
        if(animated){ infoLabel.layer.add(CATransition(), forKey: kCATransition) }
        if( animated ){
            UIView.animate(withDuration: 0.3) {
                self.doLayoutJobs()
            }
        }else{
            self.doLayoutJobs()
        }
    }
    
    func doLayoutJobs(){
        if(self.superview != nil) {
            if(self.superview?.superview != nil){
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.superview!.superview!.setNeedsLayout()
                self.superview!.superview!.layoutIfNeeded()
                
            }else{
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.superview!.setNeedsLayout()
                self.superview!.layoutIfNeeded()
            }
        }else{
            self.invalidateIntrinsicContentSize()
            self.setNeedsLayout()
        }
    }
    
    override open var intrinsicContentSize : CGSize {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let calculatedSize = CGSize(width: self.frame.size.width, height: infoLabel.frame.maxY + bottomPadding)
        return calculatedSize
    }
    
    public override func layoutSubviews() {
        let width: CGFloat = self.frame.size.width
        
        let infoLabelSize = self.infoLabel.sizeThatFits(CGSize(width: width-leftPadding-rightPadding, height: 10000))
        infoLabel.frame = CGRect(x: leftPadding, y: topPadding, width: infoLabelSize.width, height: infoLabelSize.height)
    }
}
