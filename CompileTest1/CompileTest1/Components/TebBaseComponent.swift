//
//  TebComponent.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit
import TPKeyboardAvoiding

public enum TebComponentState: Int {
    case emptyDisabled = 1
    case filledDisabled = 2
    case empty = 3
    case filled = 4
    case editing = 5
    case emptyInvalid = 6
    case filledInvalid = 7
    case editingInvalid = 8
}

enum TebValidateState: Int {
    case onEndEditing = 1
    case onChange = 2
}

@objc(TebTextFieldDelegate)
public protocol TebTextFieldDelegate: class {
    @objc optional func tebTextField(editingDidBegin textField: UITextField)
    @objc optional func tebTextField(editingChanged textField: UITextField)
    @objc optional func tebTextField(editingDidEnd textField: UITextField)
}

public class TebBaseComponent: UIControl, Validatable {
    
    // Validatable
    public var isValid: Bool = true
    public var errorMessage: String? = nil
    public var checkValidation: Bool = true
    public var validators: [Validator]? = [Validator]()
    
    public func validate() {
    }
    
    public func updateComponentStateForValidation(){    
    }
    
    // UIView
    
    public var paddingLeft: CGFloat = 16
    public var paddingRight: CGFloat = 16
    
    public var colorDisabled: UIColor = UIColor.ceptetebDark20Color()
    public var colorEmpty: UIColor = UIColor.ceptetebDark40Color()
    public var colorInvalid: UIColor = UIColor.ceptetebBrightOrangeColor()
    public var colorFiled: UIColor = UIColor.ceptetebShamrockGreenColor()
    
    public var leftImageCenterXOffset: CGFloat = 0
    public var leftImageCenterYOffset: CGFloat = 0
    public var rightImageCenterXOffset: CGFloat = 0
    public var rightImageCenterYOffset: CGFloat = 0
    public var leftImageName: String? = nil
    public var rightImageName: String? = nil
    public var leftImageView: UIImageView!
    public var rightImageView: UIImageView!
    
    public var titleTextDisabled: String! = "Alıcı Banka Seçilemiyor"
    public var titleTextEmpty: String! = "Alıcı Bankayı Yazınız"
    public var titleTextEditing: String! = "Editing Title"
    public var titleTextFilled: String! = "Alıcı Banka"
    public var titleTextInvalid: String! = "Alıcı Banka"
    public var titleText: String? {
        didSet {
            titleTextDisabled = titleText
            titleTextEmpty = titleText
            titleTextEditing = titleText
            titleTextFilled = titleText
            titleTextInvalid = titleText
        }
    }
    
    private var _componentState: TebComponentState = .empty
    var componentState: TebComponentState {
        get {
            return _componentState
        }
    }
    
    func updateComponentState(newState: TebComponentState, animated: Bool){
        print("change component state from ===\(_componentState)=== to ===\(newState)===")
        if(_componentState == newState) { return }
        _componentState = newState
        
        if( animated ){
            UIView.animate(withDuration: 0.3) {
                self.doLayoutJobs()
            }
        }else{
            self.doLayoutJobs()
        }
    }
    
    func doLayoutJobs(){
        self.updateProperties(animated: false)
        if(self.superview != nil) {
            if(self.superview?.superview != nil){
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.superview!.superview!.setNeedsLayout()
                self.superview!.superview!.layoutIfNeeded()
                
                if( self.componentState == .editing || self.componentState == .editingInvalid ){
                    if( self.superview?.superview is TPKeyboardAvoidingScrollView ){
                        let tp = self.superview?.superview as! TPKeyboardAvoidingScrollView
                        tp.tpKeyboardAvoiding_scrollToActiveTextField()
                    }
                }
                
            }else{
                self.invalidateIntrinsicContentSize()
                self.setNeedsLayout()
                self.superview!.setNeedsLayout()
                self.superview!.layoutIfNeeded()
            }
        }else{
            self.invalidateIntrinsicContentSize()
            self.setNeedsLayout()
        }
    }
    
    var validateState: TebValidateState = .onEndEditing
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
        didLoad()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func didLoad(){
        
        if( leftImageView == nil ){
            leftImageView = UIImageView()
            leftImageView.contentMode = .center
            if(DevelopmentHelper.markComponentLevel2){ leftImageView.backgroundColor = UIColor.markColorBg2() }
            self.addSubview(leftImageView)
            
            rightImageView = UIImageView()
            rightImageView.contentMode = .center
            if(DevelopmentHelper.markComponentLevel2){ leftImageView.backgroundColor = UIColor.markColorBg2() }
            self.addSubview(rightImageView)
        }
        
    }
    
    func updateProperties(animated: Bool){
        
        if( leftImageName != nil ){
            var trueLeftImageName = leftImageName!
            trueLeftImageName = trueLeftImageName.replacingOccurrences(of: "Black", with: "")
            trueLeftImageName = trueLeftImageName.replacingOccurrences(of: "Color", with: "Black")
            trueLeftImageName = trueLeftImageName.replacingOccurrences(of: "Black", with: "")
            trueLeftImageName = "\(trueLeftImageName)Black"
            let leftImage = UIImage(named: trueLeftImageName)
            leftImageView.image = leftImage?.withRenderingMode(.alwaysTemplate)
            
            switch componentState {
            case .emptyDisabled, .filledDisabled:
                leftImageView.tintColor = colorDisabled
            case .empty:
                leftImageView.tintColor = colorEmpty
            case .filled, .editing:
                leftImageView.tintColor = colorFiled
            case .emptyInvalid, .filledInvalid, .editingInvalid:
                leftImageView.tintColor = colorInvalid
            }
            
            leftImageView.layer.add(CATransition(), forKey: kCATransition)
        }
        
        if( rightImageName != nil ){
            var trueRightImageName = rightImageName!
            trueRightImageName = trueRightImageName.replacingOccurrences(of: "Black", with: "")
            trueRightImageName = trueRightImageName.replacingOccurrences(of: "Color", with: "Black")
            trueRightImageName = trueRightImageName.replacingOccurrences(of: "Black", with: "")
            trueRightImageName = "\(trueRightImageName)Black"
            rightImageView.image = UIImage(named: rightImageName!)
            let rightImage = UIImage(named: rightImageName!)
            rightImageView.image = rightImage?.withRenderingMode(.alwaysTemplate)
            
            switch componentState {
            case .emptyDisabled, .filledDisabled:
                rightImageView.tintColor = colorDisabled
            case .empty:
                rightImageView.tintColor = colorEmpty
            case .filled, .editing:
                rightImageView.tintColor = colorFiled
            case .emptyInvalid, .filledInvalid, .editingInvalid:
                rightImageView.tintColor = colorInvalid
            }
            
            rightImageView.layer.add(CATransition(), forKey: kCATransition)
        }
        
    }
}
