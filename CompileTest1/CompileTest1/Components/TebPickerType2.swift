//
//  PickerType2.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 31/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

enum TebPickerType2State: Int {
    case emptyDisabled = 1
    case empty = 2
    case selected = 3
    case selectedInvalid = 4
}

class TebPickerType2: UIControl, UITextFieldDelegate {
    
    var minHeight: CGFloat { get { return 74-8+paddingBottom-11+paddingTop  } }
    
    private var lblPlaceholder: UILabel!
    private var lblTitle: UILabel!
    private var imgRight: UIImageView!
    private var vwLineBottom: UIView!
    private var lblInvalidDescription : UILabel!
    
    var paddingLeft: CGFloat = 16 // 16
    var paddingRight: CGFloat = 16 // 16
    var paddingTop: CGFloat = 11 // 11
    var paddingBottom: CGFloat = 8 // 8
    
    var pickerState: TebPickerType2State = .empty
    var imgRightXOffset: CGFloat = 5
    var imgRightYOffset: CGFloat = 0
    
    var bottomLineColorDisabled: UIColor = UIColor.ceptetebDark40Color()
    var bottomLineColorEmpty: UIColor = UIColor.ceptetebDark40Color()
    var bottomLineColorSelected: UIColor = UIColor.ceptetebShamrockGreenColor()
    var bottomLineColorSelectedInvalid: UIColor = UIColor.ceptetebBrightOrangeColor()
    
    var imageRightDisabled: String? = "arrow_right_green"
    var imageRightEmpty: String? = "arrow_right_green"
    var imageRightSelected: String? = "arrow_right_green"
    var imageRightSelectedInvalid: String? = "arrow_right_green"
    
    var placeholderTextDisabled: String! = "Alıcı Banka Seçilemiyor"
    var placeholderTextEmpty: String! = "Alıcı Banka Seçiniz"
    var placeholderTextSelected: String! = "Alıcı Banka"
    var placeholderTextSelectedInvalid: String! = "Alıcı Banka"
    
    var text: String! = "TEB" { didSet { lblTitle.text = text } }
    var invalidDescriptionText: String! = "Birşeyler ters gitti!" { didSet { lblInvalidDescription.text = invalidDescriptionText } }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    func didLoad(){
        lblPlaceholder = UILabel(frame: CGRect.zero)
        lblPlaceholder.text = placeholderTextEmpty
        lblPlaceholder.font = UIFont.brandonRegular14
        if(DevelopmentHelper.markComponentLevel2){ lblPlaceholder.backgroundColor = UIColor.markColorBg2() }
        lblPlaceholder.textColor = UIColor.ceptetebDark40Color()
        self.addSubview(lblPlaceholder)
        
        lblTitle = UILabel(frame: CGRect.zero)
        lblTitle.text = text
        lblTitle.font = UIFont.brandonRegular16
        lblTitle.textColor = UIColor.ceptetebDark80Color()
        if(DevelopmentHelper.markComponentLevel2){ lblTitle.backgroundColor = UIColor.markColorBg2() }
        lblTitle.textColor = UIColor.ceptetebDarkGreyBlueColor()
        self.addSubview(lblTitle)
        
        vwLineBottom = UIView(frame: CGRect.zero)
        vwLineBottom.backgroundColor = UIColor.ceptetebDark40Color()
        self.addSubview(vwLineBottom)
        
        imgRight = UIImageView(frame: CGRect.zero)
        if(DevelopmentHelper.markComponentLevel2){ imgRight.backgroundColor = UIColor.markColorBg2() }
        self.addSubview(imgRight)
        
        lblInvalidDescription = UILabel(frame: CGRect.zero)
        lblInvalidDescription.text = placeholderTextEmpty
        lblInvalidDescription.font = UIFont.brandonRegular14
        lblInvalidDescription.numberOfLines = 0
        if(DevelopmentHelper.markComponentLevel2){ lblInvalidDescription.backgroundColor = UIColor.markColorBg2() }
        lblInvalidDescription.textColor = UIColor.ceptetebBrightOrangeColor()
        self.addSubview(lblInvalidDescription)
        
        if(DevelopmentHelper.markComponentLevel1){ self.backgroundColor = UIColor.markColorBg1() }
        updatePickerState()
    }
    
    func updateState(state: TebPickerType2State, animated: Bool){
        if(state != pickerState){
            pickerState = state
        }
        if(self.superview != nil ){
            if(animated){
                UIView.animate(withDuration: 0.4, animations: {
                    self.updatePickerState()
                    self.superview?.setNeedsLayout()
                    self.superview?.layoutIfNeeded()
                })
            }else{
                self.updatePickerState()
                self.superview?.setNeedsLayout()
                self.superview?.layoutIfNeeded()
            }
        }else {
            if(animated){
                UIView.animate(withDuration: 0.4, animations: {
                    self.updatePickerState()
                })
            }else{
                updatePickerState()
            }
        }
    }
    
    func updatePlaceholderText() {
        if(pickerState == .emptyDisabled){
            lblPlaceholder.text = placeholderTextDisabled
        }else if(pickerState == .empty){
            lblPlaceholder.text = placeholderTextEmpty
        }else if(pickerState == .selected){
            lblPlaceholder.text = placeholderTextSelected
        }else if(pickerState == .selectedInvalid){
            lblPlaceholder.text = placeholderTextSelectedInvalid
        }
    }
    
    func updateRightImage(){
        if(pickerState == .emptyDisabled){
            imgRight.image = imageRightDisabled != nil ? UIImage(named:imageRightDisabled!) : nil
        }else if(pickerState == .empty){
            imgRight.image = imageRightEmpty != nil ? UIImage(named:imageRightEmpty!) : nil
        }else if(pickerState == .selected){
            imgRight.image = imageRightSelected != nil ? UIImage(named:imageRightSelected!) : nil
        }else if(pickerState == .selectedInvalid){
            imgRight.image = imageRightSelectedInvalid != nil ? UIImage(named:imageRightSelectedInvalid!) : nil
        }
        imgRight.layer.add(CATransition(), forKey: kCATransition)
    }
    
    func updateProperties(){
        if(pickerState == .emptyDisabled){
            lblInvalidDescription.alpha = 0
            lblTitle.alpha = 0
            lblPlaceholder.font = UIFont.brandonRegular16
            lblPlaceholder.text = placeholderTextDisabled
            vwLineBottom.backgroundColor = bottomLineColorDisabled
        }else if(pickerState == .empty){
            lblInvalidDescription.alpha = 0
            lblTitle.alpha = 0
            lblPlaceholder.font = UIFont.brandonRegular16
            lblPlaceholder.text = placeholderTextEmpty
            vwLineBottom.backgroundColor = bottomLineColorEmpty
        }else if(pickerState == .selected){
            lblInvalidDescription.alpha = 0
            lblTitle.alpha = 1
            lblPlaceholder.font = UIFont.brandonRegular14
            lblPlaceholder.text = placeholderTextSelected
            vwLineBottom.backgroundColor = bottomLineColorSelected
        }else if(pickerState == .selectedInvalid){
            lblInvalidDescription.alpha = 1
            lblTitle.alpha = 1
            lblPlaceholder.font = UIFont.brandonRegular14
            lblPlaceholder.text = placeholderTextSelected
            vwLineBottom.backgroundColor = bottomLineColorSelectedInvalid
        }
    }
    
    func updatePickerState() {
        updateRightImage()
        remakeConstraints()
        updateProperties()
    }
    
    func remakeConstraints(){
        if(pickerState == .emptyDisabled){
            emptyConstraints()
        }else if(pickerState == .empty){
            emptyConstraints()
        }else if(pickerState == .selected){
            selectedConstraints()
        }else if(pickerState == .selectedInvalid){
            selectedInvalidConstrainsts()
        }
    }
    
    func emptyConstraints() {
        
        clearContraints()
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.left.equalTo(vwLineBottom.snp.left)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.right.equalTo(-paddingRight)
            make.left.equalToSuperview().offset(paddingLeft)
            make.height.equalTo(1)
            make.bottom.equalToSuperview().offset(-paddingBottom)
        }
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop).priority(100)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.left.equalTo(vwLineBottom.snp.left)
            make.centerY.equalTo(lblPlaceholder.snp.centerY)
            make.width.lessThanOrEqualTo(lblPlaceholder)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
    }
    
    func selectedConstraints() {
        
        clearContraints()
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.left.equalTo(vwLineBottom.snp.left)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.right.equalTo(-paddingRight)
            make.left.equalToSuperview().offset(paddingLeft)
            make.height.equalTo(1)
            make.bottom.equalToSuperview().offset(-paddingBottom)
        }
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop)
            make.bottom.equalTo(lblTitle.snp.top).offset(-4).priority(900)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
    }
    
    func selectedInvalidConstrainsts(){
        
        clearContraints()
        
        lblInvalidDescription.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualToSuperview().offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.top.equalTo(vwLineBottom.snp.bottom).offset(8)
            make.bottom.equalToSuperview().offset(-paddingBottom)
        }
        
        vwLineBottom.snp.makeConstraints { (make) in
            make.right.equalTo(-paddingRight)
            make.left.equalToSuperview().offset(paddingLeft)
            make.height.equalTo(1)
        }
        
        lblPlaceholder.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.top.greaterThanOrEqualToSuperview().offset(paddingTop)
            make.bottom.equalTo(lblTitle.snp.top).offset(-4).priority(900)
        }
        
        lblTitle.snp.makeConstraints { (make) in
            make.right.lessThanOrEqualTo(imgRight.snp.left).offset(-5)
            make.left.equalTo(vwLineBottom.snp.left)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-8)
        }
        
        imgRight.snp.makeConstraints { (make) in
            make.width.equalTo(20)
            make.height.equalTo(20)
            make.right.equalTo(vwLineBottom.snp.right).offset(imgRightXOffset)
            make.bottom.equalTo(vwLineBottom.snp.top).offset(-10+imgRightYOffset)
        }
        
    }
    
    func clearContraints(){
        lblInvalidDescription.snp.removeConstraints()
        vwLineBottom.snp.removeConstraints()
        lblPlaceholder.snp.removeConstraints()
        lblTitle.snp.removeConstraints()
        imgRight.snp.removeConstraints()
    }
    
    deinit {
        self.removeTarget(nil, action: nil, for: .allEvents)
    }
}
