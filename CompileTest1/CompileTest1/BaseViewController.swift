//
//  BaseViewController.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 01/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit


/*
extension UIViewController {
    
    func removeObserver(observer: AnyObject, notificationName: String) {
        NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: notificationName), object: nil)
    }
}

extension UIViewController {
    
    typealias KeyboardHeightClosure = (CGFloat) -> ()
    
    func addKeyboardChangeFrameObserver(willShow willShowClosure: KeyboardHeightClosure?,
                                        willHide willHideClosure: KeyboardHeightClosure?) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil, queue: OperationQueue.main, using: { [weak self](notification) in
                                                                    if let userInfo = notification.userInfo,
                                                                        let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                                                                        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
                                                                        let c = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
                                                                        let kFrame = self?.view.convert(frame, from: nil),
                                                                        let kBounds = self?.view.bounds {
                                                                        
                                                                        let animationType = UIViewAnimationOptions(rawValue: c)
                                                                        let kHeight = kFrame.size.height
                                                                        UIView.animate(withDuration: duration, delay: 0, options: animationType, animations: {
                                                                            if kBounds.intersects(kFrame) { // keyboard will be shown
                                                                                willShowClosure?(kHeight)
                                                                            } else { // keyboard will be hidden
                                                                                willHideClosure?(kHeight)
                                                                            }
                                                                            }, completion: nil)
                                                                    } else {
                                                                        print("Invalid conditions for UIKeyboardWillChangeFrameNotification")
                                                                    }
            })
    }
    
    func removeKeyboardObserver() {
        //removeObserver(self, notificationName: UIKeyboardWillChangeFrameNotification)
    }
}
 */

class BaseViewController : UIViewController {
    
    /*
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillShowNotification(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardWillHideNotification(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        addKeyboardChangeFrameObserver(willShow: { [weak self](height) in
            //Update constraints here
            self?.view.setNeedsUpdateConstraints()
            }, willHide: { [weak self](height) in
                //Reset constraints here
                self?.view.setNeedsUpdateConstraints()
            })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShowNotification( notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification: notification)
    }
    
    func keyboardWillHideNotification( notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification: notification)
    }
 */
    
    //func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
        
        
        /*
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        let rawAnimationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uint32Value << 16
        //let animationCurve =  UIViewAnimationOptions.(UInt(rawAnimationCurve))!
        let animationCurve = UIViewAnimationOptions(rawValue: UInt(rawAnimationCurve))
        bottomLayoutConstraint.constant = CGRectGetMaxY(view.bounds) - CGRectGetMinY(convertedKeyboardEndFrame)
        
        UIView.animate(withDuration: animationDuration, delay: 0, options: [ .beginFromCurrentState, animationCurve ], animations: {
            self.view.layoutIfNeeded()
        }) { (isCompleted: Bool) in
            
        }
        */
        
        /*
        UIView.animateWithDuration(animationDuration, delay: 0.0, options: .BeginFromCurrentState | animationCurve, animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
 */
    //}

    
    /*
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWillShowNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification)
    }
    
    func keyboardWillHideNotification(notification: NSNotification) {
        updateBottomLayoutConstraintWithNotification(notification)
    }
    
    
    // MARK: - Private
    
    func updateBottomLayoutConstraintWithNotification(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as NSValue).CGRectValue()
        let convertedKeyboardEndFrame = view.convertRect(keyboardEndFrame, fromView: view.window)
        let rawAnimationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as NSNumber).unsignedIntValue << 16
        let animationCurve = UIViewAnimationOptions.fromRaw(UInt(rawAnimationCurve))!
        
        UIView.animateWithDuration(animationDuration, delay: 0.0, options: .BeginFromCurrentState | animationCurve, animations: {
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
 */
    
}
