//
//  UIKit+ValidatableUserInterfaceElement.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import UIKit

public extension UIView {
    
    private func getValidatableInterfaceElements() -> Array<ValidatableInterfaceElement> {
        
        var views = Array<ValidatableInterfaceElement>()
        
        for subview in self.subviews {
            
            if subview is ValidatableInterfaceElement {
                views.append(subview as! ValidatableInterfaceElement)
                continue
            }
            views.append(contentsOf: subview.getValidatableInterfaceElements())
        }
        return views
    }
    
    public func validateUIElements(updateAppearance: Bool) -> Bool {
        
        var isValid = true
        
        for uielement in self.getValidatableInterfaceElements() {
            isValid = uielement.validate(updateAppearance: updateAppearance).isValid && isValid
        }
        
        return isValid
    }
    
    public func findTebFormScrollView() -> TebFormScrollView? {
        
        var scrollView: TebFormScrollView?
        
        for subview in self.subviews {
            
            if subview is TebFormScrollView {
                scrollView = (subview as! TebFormScrollView)
                break
            }
            scrollView = subview.findTebFormScrollView()
        }
        return scrollView
    }
    
    func findFirstInvalidInterfaceElement() -> ValidatableInterfaceElement? {
        
        for element in self.getValidatableInterfaceElements() {
            if element.validate(updateAppearance: false).isValid {
                continue
            }
            return element
        }
        return nil
    }
}

extension UITextField: ValidatableInterfaceElement {
    
    open var inputValue: ValidatableInput? { return text }
    
    open func validateOnInputChange(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingChanged)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingChanged)
        }
    }
    
    open func validateOnEditingEnd(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingDidEnd)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingDidEnd)
        }
    }
    
    @objc internal func validate(sender: UITextField) {
        sender.validate()
    }
}


extension UISwitch: ValidatableInterfaceElement {
    
    open var inputValue: ValidatableInput? { return isOn }
    
    open func validateOnInputChange(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .valueChanged)
        case false:
            removeTarget(self, action: #selector(validate), for: .valueChanged)
        }
    }
    
    open func validateOnEditingEnd(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .valueChanged)
        case false:
            removeTarget(self, action: #selector(validate), for: .valueChanged)
        }
    }
    
    @objc internal func validate(sender: UISwitch) {
        sender.validate()
    }
}

/*
extension CeptetebTextFieldCheckBox: ValidatableInterfaceElement {
    
    open var inputValue: ValidatableInput? { return text }
    
    open func validateOnInputChange(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingChanged)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingChanged)
        }
    }
    
    open func validateOnEditingEnd(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingDidEnd)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingDidEnd)
        }
    }
    
    @objc internal func validate(sender: CeptetebTextFieldCheckBox) {
        sender.validate()
    }
}

extension CeptetebTextFieldRadioButton: ValidatableInterfaceElement {
    
    open var inputValue: ValidatableInput? { return text }
    
    open func validateOnInputChange(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingChanged)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingChanged)
        }
    }
    
    open func validateOnEditingEnd(enabled: Bool) {
        
        switch enabled {
        case true:
            addTarget(self, action: #selector(validate), for: .editingDidEnd)
        case false:
            removeTarget(self, action: #selector(validate), for: .editingDidEnd)
        }
    }
    
    @objc internal func validate(sender: CeptetebTextFieldRadioButton) {
        sender.validate()
    }
}
 */

