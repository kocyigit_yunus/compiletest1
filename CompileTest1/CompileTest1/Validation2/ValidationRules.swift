//
//  ValidationRules.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation

public class _EmailValidator: ValidationRule {
    
    public var error: Error
    
    public init() {
        self.error = ValidationError(message: NSLocalizedString("error_invalid_email", comment: ""))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let stringInput = input as? String else { return false }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringInput)
    }
}

public class _ExactLengthValidator: ValidationRule {
    
    public var error: Error
    
    var exactLength: Int
    
    public init(exactLength: Int) {
        self.exactLength = exactLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_exact_length", comment: ""), String(exactLength)))
    }
    
    public init(exactLength: Int, error: ValidationError) {
        self.exactLength = exactLength
        self.error = error
    }
    
    public init(exactLength: Int, subject: String) {
        self.exactLength = exactLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_exact_length_with_subject", comment: ""), subject, String(exactLength)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let stringInput = input as? String else { return false }
        return stringInput.characters.count == self.exactLength
    }
}

public class _MinLengthValidator: ValidationRule {
    
    public var error: Error
    
    var minLength: Int
    
    public init(minLength: Int) {
        self.minLength = minLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_min_length", comment: "?"), String(minLength)))
    }
    
    public init(minLength: Int, error: ValidationError) {
        self.minLength = minLength
        self.error = error
    }
    
    public init(minLength: Int, subject: String) {
        self.minLength = minLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_min_length_with_subject", comment: ""), subject, String(minLength)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        
        guard let stringInput = input as? String else { return false }
        
        let trimmedString = stringInput.trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmedString.characters.count >= self.minLength
    }
}

/******/

public class _MaxLengthValidator: ValidationRule {
    
    public var error: Error
    
    var maxLength: Int
    
    public init(maxLength: Int) {
        self.maxLength = maxLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_max_length", comment: ""), String(maxLength)))
    }
    
    public init(maxLength: Int, error: ValidationError) {
        self.maxLength = maxLength
        self.error = error
    }
    
    public init(maxLength: Int, subject: String) {
        self.maxLength = maxLength
        self.error = ValidationError(message: String(format: NSLocalizedString("error_max_length_with_subject", comment: ""), subject, String(maxLength)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let stringInput = input as? String else { return false }
        return stringInput.characters.count <= self.maxLength
    }
}

public class _MinValueValidator: ValidationRule {
    
    public var error: Error
    
    var minValue: Double
    
    public init(minValue: Double) {
        self.minValue = minValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_min_amount", comment: ""), String(minValue)))
    }
    
    public init(minValue: Double, error: ValidationError) {
        self.minValue = minValue
        self.error = error
    }
    
    public init(minValue: Double, subject: String) {
        self.minValue = minValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_min_amount_with_subject", comment: ""), subject, String(minValue)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let input = input as? String, let doubleVal = Double(input) else { return false }
        return doubleVal >= self.minValue
    }
}

public class _MaxValueValidator: ValidationRule {
    
    public var error: Error
    
    var maxValue: Double
    
    public init(maxValue: Double) {
        self.maxValue = maxValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_max_amount", comment: ""), String(maxValue)))
    }
    
    public init(maxValue: Double, error: ValidationError) {
        self.maxValue = maxValue
        self.error = error
    }
    
    public init(maxValue: Double, subject: String) {
        self.maxValue = maxValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_max_amount_with_subject", comment: ""), subject, String(maxValue)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let input = input as? String, let doubleVal = Double(input) else { return false }
        return doubleVal <= self.maxValue
    }
}

public class _PlakaValidator: ValidationRule {
    
    public var error: Error
    
    var separator: String
    
    public init(separator: String) {
        self.separator = separator
        self.error = ValidationError(message: NSLocalizedString("gecersiz_plaka", comment: ""))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let input = input as? String else { return false }
        let parts = input.components(separatedBy: separator)
        var isValid = true
        if parts.count == 3 {
            if parts[0].characters.count != 2 {
                isValid = false
            } else if parts[1].characters.count < 1 || parts[1].characters.count > 3 {
                isValid = false
            } else if parts[2].characters.count < 2 || parts[2].characters.count > 5 {
                isValid = false
            }
        } else {
            isValid = false
        }
        return isValid
    }
}

public class _BoolValidator: ValidationRule {
    
    public var error: Error
    
    var boolValue: Bool
    
    public init(boolValue: Bool) {
        self.boolValue = boolValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_bool_value", comment: ""), String(boolValue)))
    }
    
    public init(boolValue: Bool, error: ValidationError) {
        self.boolValue = boolValue
        self.error = error
    }
    
    public init(boolValue: Bool, subject: String) {
        self.boolValue = boolValue
        self.error = ValidationError(message: String(format: NSLocalizedString("error_bool_value_with_subject", comment: ""), subject, String(boolValue)))
    }
    
    public func validate(input: ValidatableInput?) -> Bool {
        guard let boolInput = input as? Bool else { return false }
        return boolValue == boolInput
    }
}
