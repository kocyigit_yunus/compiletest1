//
//  ValidatableInterfaceElement.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation

public protocol ValidatableInterfaceElement: AnyObject {
    
    var inputValue: ValidatableInput? { get }
    
    var validationHandler: ((ValidationResult) -> Void)? { get set }
    
    func validate(rules: Array<ValidationRule>, updateAppearance: Bool) -> ValidationResult
    
    func validateOnInputChange(enabled: Bool)
    
    func validateOnEditingEnd(enabled: Bool)
    
    var shouldValidate: Bool { get set }

}

private var ValidatableInterfaceElementRulesKey: UInt8 = 0

private var ValidatableInterfaceElementHandlerKey: UInt8 = 0

private var ValidatableInterfaceElementShouldValidateKey: UInt8 = 0


extension ValidatableInterfaceElement {
    
    public var validators: Array<ValidationRule>? {
        
        get {
            return objc_getAssociatedObject(self, &ValidatableInterfaceElementRulesKey) as? Array<ValidationRule>
        }
        
        set(newValue) {
            if let n = newValue {
                objc_setAssociatedObject(self, &ValidatableInterfaceElementRulesKey, n as AnyObject, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    public var validationHandler: ((ValidationResult) -> Void)? {
        get {
            return objc_getAssociatedObject(self, &ValidatableInterfaceElementHandlerKey) as? (ValidationResult) -> Void
        }
        
        set(newValue) {
            if let n = newValue {
                objc_setAssociatedObject(self, &ValidatableInterfaceElementHandlerKey, n as AnyObject, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    public var shouldValidate: Bool {
        
        get {
            if let shouldValidate = objc_getAssociatedObject(self, &ValidatableInterfaceElementShouldValidateKey) as? Bool {
                return shouldValidate
            }
            return true
        }
        
        set(newValue) {
            objc_setAssociatedObject(self, &ValidatableInterfaceElementShouldValidateKey, newValue as AnyObject, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    public func validate(rules rs: Array<ValidationRule>, updateAppearance: Bool) -> ValidationResult {
        
        if shouldValidate == false {
            return .valid
        }
        
        let result = inputValue!.validate(rules: rs)
        
        if let h = validationHandler {
            if updateAppearance {
                h(result)
            }
        }
        return result
    }
    
    @discardableResult public func validate(updateAppearance: Bool = true) -> ValidationResult {
        
        guard let attachedRules = validators else {
            #if DEBUG
                print("Validator Error: attempted to validate without attaching rules")
            #endif
            return .valid
        }
        return validate(rules: attachedRules, updateAppearance: updateAppearance)
    }
}
