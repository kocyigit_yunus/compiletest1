//
//  ValidationResult.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation

public enum ValidationResult: Equatable {
    
    case valid
    case invalid(Array<Error>)
    
    public var isValid: Bool { return self == .valid }
    
    public static func ==(lhs: ValidationResult, rhs: ValidationResult) -> Bool {
        
        switch (lhs, rhs) {
            
        case (.valid, .valid): return true
            
        case (.invalid(_), .invalid(_)): return true
            
        default: return false
            
        }
    }
}

public struct ValidationError: Error {
    public var message: String
}
