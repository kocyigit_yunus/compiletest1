//
//  Validatable.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation

public protocol ValidatableInput {
    func validate(rules: Array<ValidationRule>) -> ValidationResult
}

extension ValidatableInput {
    public func validate(rules: Array<ValidationRule>) -> ValidationResult {
        let errors = rules.filter { !$0.validate(input: self) }.map { $0.error }
        return errors.isEmpty ? ValidationResult.valid : ValidationResult.invalid(errors)
    }
}

extension String : ValidatableInput {}

extension Int : ValidatableInput {}

extension Double : ValidatableInput {}

extension Float : ValidatableInput {}

extension Date : ValidatableInput {}

extension Bool : ValidatableInput {}
