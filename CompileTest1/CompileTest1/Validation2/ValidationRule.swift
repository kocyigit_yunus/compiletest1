//
//  ValidationRule.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 13/01/2017.
//  Copyright © 2017 TEB A.Ş. All rights reserved.
//

import Foundation

public protocol ValidationRule {
    
    func validate(input: ValidatableInput?) -> Bool
    
    var error: Error { get }
}
