//
//  UIFontExtensions.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 10/08/16.
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

extension UIFont {
    
    // MARK: - Private common factory method.
    
    fileprivate class func getFont(_ name: String, size: CGFloat) -> UIFont {
        return UIFont(name: name, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    // MARK: - Public Methods
    
    public func stringSize(_ string: String, constrainedToWidth width: Double) -> CGSize {
        return string.boundingRect(with: CGSize(width: width, height: DBL_MAX), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: self], context: nil).size
    }
    
    // MARK: - Cepteteb Custom Fonts
    
    // Monospaced font, sadece sayısal alanlarda kullanılacak.
    var monospacedDigitFont: UIFont {
        let oldFontDescriptor = fontDescriptor
        let newFontDescriptor = oldFontDescriptor.monospacedDigitFontDescriptor
        return UIFont(descriptor: newFontDescriptor, size: 0)
    }
    
    // MARK - BrandonGrotesque Font, proportional by default
    
    class func BrandonGrotesqueBlack(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-Black", size: size)
    }
    
    class func BrandonGrotesqueBlackItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-BlackItalic", size: size)
    }
    
    class func BrandonGrotesqueBold(_ size: CGFloat) -> UIFont {
        return getFont("BrandonText-Bold", size: size)
    }
    
    class func BrandonGrotesqueBoldItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-BoldItalic", size: size)
    }
    
    class func BrandonGrotesqueLight(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-Light", size: size)
    }
    
    class func BrandonGrotesqueLightItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-LightItalic", size: size)
    }
    
    class func BrandonGrotesqueMedium(_ size: CGFloat) -> UIFont {
        return getFont("BrandonText-Medium", size: size)
    }
    
    class func BrandonGrotesqueMediumItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-MediumItalic", size: size)
    }
    
    class func BrandonGrotesqueRegular(_ size: CGFloat) -> UIFont {
        return getFont("BrandonText-Regular", size: size)
    }
    
    class func BrandonGrotesqueRegularItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-RegularItalic", size: size)
    }
    
    class func BrandonGrotesqueThin(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-Thin", size: size)
    }
    
    class func BrandonGrotesqueThinItalic(_ size: CGFloat) -> UIFont {
        return getFont("BrandonGrotesque-ThinItalic", size: size)
    }
    
    // MARK: - ChronicleTextG1, proportional by default.
    
    class func ChronicleTextG1Bold(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-Bold", size: size)
    }
    
    class func ChronicleTextG1Semi(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-Semi", size: size)
    }
    
    class func ChronicleTextG1Italic(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-Italic", size: size)
    }
    
    class func ChronicleTextG1BoldItalic(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-BoldItalic", size: size)
    }
    
    class func ChronicleTextG1Roman(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-Roman", size: size)
    }
    
    class func ChronicleTextG1SemiItalic(_ size: CGFloat) -> UIFont {
        return getFont("ChronicleTextG1-SemiItalic", size: size)
    }
    
    static func getBrandonRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Regular", size: size)!
    }
    
    static func getBrandonMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Medium", size: size)!
    }
    
    static func getBrandonBold(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Bold", size: size)!
    }
    
    static var brandonRegular14: UIFont { get { return getBrandonRegular(size: 14) } }
    static var brandonRegular16: UIFont { get { return getBrandonRegular(size: 16) } }
    static var brandonRegular18: UIFont { get { return getBrandonRegular(size: 18) } }
    static var brandonRegular20: UIFont { get { return getBrandonRegular(size: 20) } }
    
    static var brandonMedium14: UIFont { get { return getBrandonMedium(size: 14) } }
    static var brandonMedium16: UIFont { get { return getBrandonMedium(size: 16) } }
    static var brandonMedium18: UIFont { get { return getBrandonMedium(size: 18) } }
    static var brandonMedium20: UIFont { get { return getBrandonMedium(size: 20) } }
    
    static var brandonBold14: UIFont { get { return getBrandonBold(size: 14) } }
    static var brandonBold16: UIFont { get { return getBrandonBold(size: 16) } }
    static var brandonBold18: UIFont { get { return getBrandonBold(size: 18) } }
    static var brandonBold20: UIFont { get { return getBrandonBold(size: 20) } }
}

private extension UIFontDescriptor {
    
    var monospacedDigitFontDescriptor: UIFontDescriptor {
        let fontDescriptorFeatureSettings = [[UIFontFeatureTypeIdentifierKey: kNumberSpacingType, UIFontFeatureSelectorIdentifierKey: kMonospacedNumbersSelector]]
        let fontDescriptorAttributes = [UIFontDescriptorFeatureSettingsAttribute: fontDescriptorFeatureSettings]
        let fontDescriptor = self.addingAttributes(fontDescriptorAttributes)
        return fontDescriptor
    }
}
