//
//  Util.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 29/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit


/*
 delay(0.4) { bla bla bla; }
 #fasterDevelopment
 */
func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

class DevelopmentHelper {
    static var markComponentLevel1: Bool = true
    static var markComponentLevel2: Bool = true
    static var markComponentLevel3: Bool = false
}

class Color {
    
    static var redColor : UIColor { get { return UIColor.red } }
    static var greenColor : UIColor { get { return UIColor.green } }
    static var blueColor : UIColor { get { return UIColor.blue } }
    static var whiteColor : UIColor { get { return UIColor.white } }
    
    //static var color1LightGreen: UIColor { get { return UIColor(red: 0/255, green: 225/255, blue: 75/255, alpha: 1) } }
    //static var color2DarkGreen: UIColor { get { return UIColor(red: 0/255, green: 175/255, blue: 75/255, alpha: 1) } }
    //static var color3Orange: UIColor { get { return UIColor(red: 255/255, green: 100/255, blue: 0/255, alpha: 1) } }
    //static var color8White: UIColor { get { return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) } }
    //static var color9White80: UIColor { get { return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.8) } }
    //static var color10White60: UIColor { get { return UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.6) } }
    
    //static var colorDarkGreyBlue: UIColor { get { return UIColor(red: 46/255, green: 66/255, blue: 77/255, alpha: 1) } }
    //static var colorDark: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 1) } }
    //static var colorDark80: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.8) } }
    //static var colorDark60: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.6) } }
    //static var colorDark40: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.4) } }
    //static var colorDark30: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.3) } }
    //static var colorDark20: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.2) } }
    //static var colorDark24: UIColor { get { return UIColor(red: 31/255, green: 44/255, blue: 51/255, alpha: 0.24) } }
    
    //static var markColorBk1: UIColor { get { return UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 0.3) } }
    //static var markColorBk2: UIColor { get { return UIColor(red:0/255, green: 255/255, blue: 0/255, alpha: 0.3) } }
    //static var markColorBk3: UIColor { get { return UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 0.3) } }
    //static var markColorBk4: UIColor { get { return UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 0.3) } }
}

/*
class Font {

    static func getBrandonRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Regular", size: size)!
    }
    
    static func getBrandonMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Medium", size: size)!
    }
    
    static func getBrandonBold(size: CGFloat) -> UIFont {
        return UIFont(name: "BrandonText-Bold", size: size)!
    }
    
    static var brandonRegular14: UIFont { get { return getBrandonRegular(size: 14) } }
    static var brandonRegular16: UIFont { get { return getBrandonRegular(size: 16) } }
    static var brandonRegular18: UIFont { get { return getBrandonRegular(size: 18) } }
    static var brandonRegular20: UIFont { get { return getBrandonRegular(size: 20) } }
    
    static var brandonMedium14: UIFont { get { return getBrandonMedium(size: 14) } }
    static var brandonMedium16: UIFont { get { return getBrandonMedium(size: 16) } }
    static var brandonMedium18: UIFont { get { return getBrandonMedium(size: 18) } }
    static var brandonMedium20: UIFont { get { return getBrandonMedium(size: 20) } }
    
    static var brandonBold14: UIFont { get { return getBrandonBold(size: 14) } }
    static var brandonBold16: UIFont { get { return getBrandonBold(size: 16) } }
    static var brandonBold18: UIFont { get { return getBrandonBold(size: 18) } }
    static var brandonBold20: UIFont { get { return getBrandonBold(size: 20) } }
    
}
 */

class Line {
    
    static func getLineWithPx(size: CGFloat) -> CGFloat {
        return size / UIScreen.main.scale
    }
    
    static var line1px: CGFloat { get { return getLineWithPx(size:1)  } }
    static var line2px: CGFloat { get { return getLineWithPx(size:2)  } }
    static var line3px: CGFloat { get { return getLineWithPx(size:3)  } }
    
}

func delay(delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}
/*
delay(delay: 1) {
    print("Hi!")
}
 */
