//
//  UIColorExtensions.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 28/06/16.
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func dashboardBackground() -> UIColor {
        return UIColor(red: 250.0 / 255.0, green: 250.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
    
    // MARK: - Updated Color Palette.
    
    class func ceptetebShamrockColor() -> UIColor {
        return UIColor(red: 0.0 / 255.0, green: 175.0 / 255.0, blue: 75.0 / 255.0, alpha: 1.0)
    }
    
    class func ceptetebShamrockGreenColor() -> UIColor {
        return UIColor(red: 0.0 / 255.0, green: 225.0 / 255.0, blue: 75.0 / 255.0, alpha: 1.0)
    }
    
    class func ceptetebBrightOrangeColor() -> UIColor {
        return UIColor(red: 255.0 / 255.0, green: 100.0 / 255.0, blue: 0.0 / 255.0, alpha: 1.0)
    }
    
    class func ceptetebDarkGreyBlueColor() -> UIColor {
        return UIColor(red: 46.0 / 255.0, green: 66.0 / 255.0, blue: 77.0 / 255.0, alpha: 1.0)
    }
    
    class func ceptetebDarkColor() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
    }
    
    class func ceptetebDark40Color() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.4)
    }
    
    class func ceptetebDark80Color() -> UIColor {
        return UIColor(red: 30.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.80)
    }
    
    class func ceptetebDarkTwo80Color() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.8)
    }
    
    class func ceptetebDark60Color() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.60)
    }
    
    class func ceptetebDark24Color() -> UIColor {
        return UIColor(red: 30.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.24)
    }
    
    class func ceptetebDark20Color() -> UIColor {
        return UIColor(red: 30.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.20)
    }
    
    class func ceptetebDark4Color() -> UIColor {
        return UIColor(red: 30.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.04)
    }
    
    class func ceptetebDark8Color() -> UIColor {
        return UIColor(red: 30.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.08)
    }
    
    class func ceptetebDark0Color() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.0)
    }
    
    class func ceptetebWhite60Color() -> UIColor {
        return UIColor(white: 255.0 / 255.0, alpha: 0.6)
    }
    
    class func fiWhiteGreyColor() -> UIColor {
        return UIColor(white: 245.0 / 255.0, alpha: 1.0)
    }
    
    class func fiDarkTwo20Color() -> UIColor {
        return UIColor(red: 31.0 / 255.0, green: 44.0 / 255.0, blue: 51.0 / 255.0, alpha: 0.2)
    }
    
    class func ceptetebShamrockGreenColor2() -> UIColor {
        return UIColor(red: 0.0 / 255.0, green: 219.0 / 255.0, blue: 73.0 / 255.0, alpha: 1.0)
    }
    
    
    // Development Helper
    class func markColorBg1() -> UIColor {
        return UIColor(red: 0/255.0, green: 0/255.0, blue: 255/255.0, alpha: 0.3)
    }
    
    class func markColorBg2() -> UIColor {
        return UIColor(red:0/255, green: 255/255, blue: 0/255, alpha: 0.3)
    }
    
    class func markColorBg3() -> UIColor {
        return UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 0.3)
    }
    
    class func markColorBg4() -> UIColor {
        return UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 0.3)
    }
    
    // Tint Colors
    class func tintColorDisabled() -> UIColor {
        return UIColor.gray
    }
    
    class func tintColorEmpty() -> UIColor {
        return UIColor.gray
    }
    
    class func tintColorFilled() -> UIColor {
        return UIColor.gray
    }
    
    class func tintColorInvalid() -> UIColor {
        return UIColor.gray
    }
}
