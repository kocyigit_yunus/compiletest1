//
//  Extensions.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 08/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func deepestFirstInvalidSubview() -> UIView? {
        
        let orderedSubviews = self.subviews.sorted(by: { return $0.frame.origin.y < $1.frame.origin.y })
        
        for subview in orderedSubviews {
            if(subview is Validatable){
                let validatableSubview = subview as! Validatable
                if(validatableSubview.isValid == false){
                    return subview
                }
            }else{
                let invalidSubview = subview.deepestFirstInvalidSubview()
                if(invalidSubview != nil){
                    return invalidSubview
                }
            }
        }
        
        if(self is Validatable){
            let validatableSelf = self as! Validatable
            if(validatableSelf.isValid == false){
                return self
            }
        }
        
        return nil
    }
    
    func validateSubviews() {
        for subview in self.subviews {
            if(subview is Validatable){
                let validatableSubview = subview as! Validatable
                _ = validatableSubview.validate()
            }else{
                subview.validateSubviews()
            }
        }
        
        if(self is Validatable){
            let validatableSelf = self as! Validatable
            _ = validatableSelf.validate()
        }
    }
    
    func findClosestParentOfView( targetView: UIView ) -> UIView? {
        
        if( self == targetView ){
            return self
        }
        
        for subview in self.subviews {
            if( subview.findClosestParentOfView(targetView: targetView) != nil) {
                return subview
            }
        }
        
        return nil
    }
    
    func updateSubviewValidationStates() {
        
        for subview in self.subviews {
            if(subview is Validatable){
                let validatableSubview = subview as! Validatable
                validatableSubview.updateComponentStateForValidation()
            }else{
                subview.updateSubviewValidationStates()
            }
        }
        
        if(self is Validatable){
            let validatableSelf = self as! Validatable
            validatableSelf.updateComponentStateForValidation()
        }
        
    }
}
