//
//  TEBValidator.swift
//  Cepteteb
//
//  Created by Kaan Çetin on 11/05/16.
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

public protocol Validatable {
    var isValid: Bool { get }
    var errorMessage: String? { get }
    var checkValidation: Bool { get set }
    var validators: [Validator]? { get set }
    func validate()
    func updateComponentStateForValidation()
}

public extension Validatable {
    var validationAnimationDuration: TimeInterval { return 0.2 }
}

public protocol Validator {
    var priority: Int { get set }
    var errorMessage: String { get set }
    func validate(value: AnyObject?) -> String?
    
    /*
     public func validate(value: AnyObject?) -> String? {
     if ( value is String ){
     let stringValue = value as! String
     return isValid ? nil : errorString
     }else{
     return errorString
     }
     }
     */
}

public class RequiredValidator: Validator {
    public var priority: Int = 500
    public var errorMessage: String = "Bu Alan Zorunludur!"
    public func validate(value: AnyObject?) -> String? {
        if( value != nil ){
            return nil
        }
        return errorMessage
    }
    
    init(errorMessage: String? = nil, priority: Int = 500) {
        self.errorMessage = errorMessage ?? self.errorMessage
        self.priority = priority
    }
}

public class EmailValidator: Validator {
    
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_invalid_email", comment: "")
    
    public init() {}
    
    public func validate(value: AnyObject?) -> String? {
        if( value is String ){
            let stringValue = value as! String
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
            let isValid = emailTest.evaluate(with: stringValue)
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
    
}

public class ExactLengthValidator: Validator {
    
    var exactLength: Int
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_exact_length", comment: "")
    
    public init(exactLength: Int) {
        self.exactLength = exactLength
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let isValid = stringValue.characters.count == self.exactLength
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
    
}

public class MinLengthValidator: Validator {
    
    var minLength: Int
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_min_length", comment: "")
    
    public init(minLength: Int) {
        self.minLength = minLength
        self.errorMessage = self.errorMessage.replacingOccurrences(of: "%@", with: String(minLength))
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let trimmedString = stringValue.trimmingCharacters(in: CharacterSet.whitespaces)
            let isValid = trimmedString.characters.count >= self.minLength
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
    
}

public class MaxLengthValidator: Validator {
    
    var maxLength: Int
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_max_length", comment: "")
    
    public init(maxLength: Int) {
        self.maxLength = maxLength
        self.errorMessage = self.errorMessage.replacingOccurrences(of: "%@", with: String(maxLength))
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let isValid = stringValue.characters.count <= self.maxLength
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
    
}

public class MinValueValidator: Validator {
    
    var minValue: Double
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_min_amount", comment: "")
    
    public init(minValue: Double) {
        self.minValue = minValue
        self.errorMessage = self.errorMessage.replacingOccurrences(of: "%@", with: String(minValue))
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let input = Double(stringValue)
            let isValid = input ?? Double(Int.min) >= self.minValue
            return isValid ? nil : errorMessage
        }else if( value is Double ){
            let input = value as! Double
            let isValid = input >= self.minValue
            return isValid ? nil : errorMessage
        }
        else{
            return errorMessage
        }
    }
    
}

public class MaxValueValidator: Validator {
    
    var maxValue: Double
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("error_max_amount", comment: "")
    
    public init(maxValue: Double) {
        self.maxValue = maxValue
        self.errorMessage = self.errorMessage.replacingOccurrences(of: "%@", with: String(maxValue))
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let input = Double(stringValue)
            let isValid = input ?? Double(Int.max) <= self.maxValue
            return isValid ? nil : errorMessage
        }else if( value is Double ){
            let input = value as! Double
            let isValid = self.maxValue >= input
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
    
}

public class PlakaValidator: Validator {
    
    var separator: String
    public var priority: Int = 500
    public var errorMessage: String = NSLocalizedString("gecersiz_plaka", comment: "")
    
    public init(separator: String) {
        self.separator = separator
    }
    
    public func validate(value: AnyObject?) -> String? {
        if ( value is String ){
            let stringValue = value as! String
            let parts = stringValue.components(separatedBy: separator)
            var isValid = true
            if parts.count == 3 {
                if parts[0].characters.count != 2 {
                    isValid = false
                } else if parts[1].characters.count < 1 || parts[1].characters.count > 3 {
                    isValid = false
                } else if parts[2].characters.count < 2 || parts[2].characters.count > 5 {
                    isValid = false
                }
            } else {
                isValid = false
            }
            return isValid ? nil : errorMessage
        }else{
            return errorMessage
        }
    }
}
