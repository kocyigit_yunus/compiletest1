//
//  ViewController1.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 01/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import UIKit

class ViewController1: BaseViewController {
    
    private var label1: UILabel!
    private var label2: UILabel!
    private var label3: UILabel!
    
    private var picker1: TebPickerType1!
    private var picker2: TebPickerType2!
    
    private var btnAction: ActionButton!
    private var txtField: UITextField!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: UIScreen.main.bounds)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        add()
        
        self.btnAction = ActionButton()
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }
        
        self.txtField = UITextField(frame: CGRect.zero)
        self.txtField.backgroundColor = UIColor.lightGray
        self.view.addSubview(self.txtField)
        self.txtField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            //make.top.equalTo(picker2.snp.bottom).offset(10)
        }
        
        _ = UITapGestureRecognizer(target: self, action:#selector(backgroundTapped(sender:)) )
        //self.view.addGestureRecognizer(gr)
    }
    
    func add(){
        
        var picker1:TebPickerType1 = TebPickerType1()
        //picker1.addTarget(self, action: #selector(tapPicker1(sender:)), for: .touchUpInside)
        
        //self.picker1.paddingLeft = 0
        //self.picker1.paddingTop = 0
        //self.picker1.paddingRight = 0
        //self.picker1.paddingBottom = 0
        
        //self.picker1.bottomLineColorDisabled = Color.colorDark40
        //self.picker1.bottomLineColorEmpty = Color.colorDark40
        //self.picker1.bottomLineColorSelected = Color.color1LightGreen
        //self.picker1.bottomLineColorSelectedInvalid = Color.color3Orange
        
        picker1.placeholderTextEmpty = "Alıcı Banka Seç"
        picker1.placeholderTextDisabled = "Alıcı Banka Seçilemiyor"
        picker1.placeholderTextSelected = "Alıcı Banka"
        picker1.placeholderTextInvalid = "Yanlış Banka!"
        
        picker1.text = "TEB"
        picker1.invalidDescriptionText = "Birşeyler ters gitti"
        
        picker1.imageRightDisabled = nil
        picker1.imageRightEmpty = "arrow_right_green"
        picker1.imageRightSelected = "arrow_right_green"
        picker1.imageRightInvalid = "arrow_right_green"
        
        picker1.imageLeftEmpty = "wallet_black"
        picker1.imageLeftSelected = "wallet_green"
        picker1.imageLeftDisabled = "wallet_black"
        picker1.imageLeftInvalid = "wallet_black"
        
        picker1.pickerState = .selected
        picker1.updateState(state: .invalid, animated: true)
        self.view.addSubview(picker1)
        
        picker1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(self.topLayoutGuide.snp.bottom).offset(10)
        }
        
        var picker2: TebPickerType2 = TebPickerType2()
        picker2.addTarget(self, action: #selector(tapPicker2(sender:)), for: .touchUpInside)
        
        //self.picker2.paddingLeft = 0
        //self.picker2.paddingTop = 0
        //self.picker2.paddingRight = 0
        //self.picker2.paddingBottom = 0
        
        //self.picker2.bottomLineColorDisabled = Color.colorDark40
        //self.picker2.bottomLineColorEmpty = Color.colorDark40
        //self.picker2.bottomLineColorSelected = Color.color1LightGreen
        //self.picker2.bottomLineColorSelectedInvalid = Color.color3Orange
        
        picker2.placeholderTextEmpty = "Alıcı Banka Seç"
        picker2.placeholderTextDisabled = "Alıcı Banka Seçilemiyor"
        picker2.placeholderTextSelected = "Alıcı Banka"
        picker2.placeholderTextSelectedInvalid = "Yanlış Banka!"
        
        picker2.text = "TEB"
        picker2.invalidDescriptionText = "Birşeyler ters gitti"
        
        picker2.imageRightDisabled = nil
        picker2.imageRightEmpty = "arrow_right_green"
        picker2.imageRightSelected = "arrow_right_green"
        picker2.imageRightSelectedInvalid = "arrow_right_green"
        
        picker2.pickerState = .selected
        picker2.updateState(state: .selectedInvalid, animated: true)
        self.view.addSubview(picker2)
        
        picker2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(picker1.snp.bottom).offset(10)
            make.height.greaterThanOrEqualTo(picker2.minHeight)
        }
        
    }
    
    func backgroundTapped( sender: AnyObject?){
        print("background tap")
        self.view.endEditing(true)
    }
    
    func tapPicker1(sender: TebPickerType1?){
        print("deneme")
        /*
        if(picker1.pickerState == .selectedInvalid) {
            picker1.updateState(state: .empty, animated: true)
        }else {
            //picker1.pickerState = .selected
            picker1.updateState(state: .selectedInvalid, animated: true)
        }
 */
    }
    
    func tapPicker2(sender: TebPickerType2?){
        print("deneme")
        /*
        if(picker1.pickerState == .selectedInvalid) {
            picker1.updateState(state: .empty, animated: true)
        }else {
            //picker1.pickerState = .selected
            picker1.updateState(state: .selectedInvalid, animated: true)
        }
         */
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

