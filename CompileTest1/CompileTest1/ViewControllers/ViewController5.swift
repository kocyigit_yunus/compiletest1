//
//  ViewController4.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class ViewController5: UIViewController, TebTextFieldDelegate {
    
    var vwScroll: TebFormScrollView!
    var vwStack: UIStackView!
    
    var txt1: TebPlainTextField!
    var txt2: TebAmountTextField!
    var picker3: TebPlainPicker!
    var txt4: TebPlainTextField!
    var txt5: TebPlainTextField!
    
    private var btnAction: ActionButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Test View Controller 5"
        
        vwScroll = TebFormScrollView(frame: CGRect.zero)
        vwScroll.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 64, right: 0)
        //vwScroll.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.15)
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.addSubview(vwScroll)
        
        self.btnAction = ActionButton()
        self.btnAction.addTarget(self, action: #selector(tapAction(sender:)), for: .touchUpInside)
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
        }
        
        vwScroll.snp.makeConstraints { (make) in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(btnAction.snp.top)
        }
        
        vwStack = UIStackView()
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .fill
        vwStack.distribution = .equalSpacing
        vwStack.spacing = 10 // 60
        vwStack.axis = .vertical
        
        vwScroll.addSubview(vwStack)
        vwStack.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(vwScroll.snp.width)
            make.height.greaterThanOrEqualTo(100)
            //make.height.greaterThanOrEqualTo(vwScroll.snp.height).priority(1000)
        }
        
        /*
        let vwSpacer1: UIView = UIView(frame: CGRect.zero)
        vwSpacer1.backgroundColor = UIColor.yellow
        vwStack.addArrangedSubview(vwSpacer1)
        vwSpacer1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(32)
        }
        */
        
        self.txt1 = TebPlainTextField()
        self.txt1.validators?.append( MinLengthValidator(minLength: 5) )
        self.txt1.leftImageName = ""
        vwStack.addArrangedSubview(self.txt1)
        self.txt1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt2 = TebAmountTextField()
        self.txt2.titleText = "Para Miktarı"
        self.txt2.leftImageName = "wallet_green"
        self.txt2.tebTextFieldDelegate = self
        self.txt2.validators?.append( MinLengthValidator(minLength: 5) )
        self.txt2.validators?.append( MinValueValidator(minValue: 2 ) )
        self.txt2.updateProperties(animated: false)
        vwStack.addArrangedSubview(self.txt2)
        self.txt2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.picker3 = TebPlainPicker()
        self.picker3.leftImageName = ""
        self.picker3.validators?.append( MinLengthValidator(minLength: 5) )
        self.picker3.titleText = "Şehir"
        self.picker3.updateProperties(animated: false)
        vwStack.addArrangedSubview(self.picker3)
        self.picker3.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt4 = TebPlainTextField()
        self.txt4.leftImageName = ""
        self.txt4.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt4)
        self.txt4.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt5 = TebPlainTextField()
        self.txt5.leftImageName = ""
        self.txt5.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt5)
        self.txt5.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        /*
        let vwSpacer2: UIView = UIView(frame: CGRect.zero)
        vwSpacer2.backgroundColor = UIColor.white
        vwStack.addArrangedSubview(vwSpacer2)
        vwSpacer2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(64)
        }
         */
        
    }
    
    func tebTextField(editingChanged textField: UITextField) {
        print("editing changed")
    }
    
    func tebTextField(editingDidBegin textField: UITextField) {
        print("editing did begin")
    }
    
    func tebTextField(editingDidEnd textField: UITextField) {
        print("editing did end")
    }
    
    func tapAction(sender: TebPickerType1?){
        
        self.view.endEditing(true)
        
        self.view.validateSubviews()
        let invalidSubview = self.view.deepestFirstInvalidSubview()
        
        if( invalidSubview != nil ){
            
            UIView.animate(withDuration: 0.3) {
                self.view.updateSubviewValidationStates()
                self.vwScroll.layoutIfNeeded()
                let yPositionOfInvalidSubview = self.vwScroll.findYPositionForView(targetView: invalidSubview!)
                self.vwScroll.setContentOffset(CGPoint(x: 0, y: yPositionOfInvalidSubview), animated: false)
            }
            
        }
        
    }
}
