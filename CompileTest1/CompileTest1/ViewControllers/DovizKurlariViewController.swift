//
//  DovizKurlariViewController.swift
//  CompileTest1
//
//  Created by Yunus Koçyiğit on 31/01/2017.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class DovizKurlariViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var tableView: UITableView!
    var reuseIdentifier = "123123"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Döviz Kurları"
        self.view.backgroundColor = UIColor.yellow
        tableView = UITableView()
        tableView.register(DovizKurlariTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier ) as? DovizKurlariTableViewCell {
            cell.detailTextLabel?.text = "123123"
            cell.detailTextLabel?.backgroundColor = UIColor.gray
            return cell
        }
        return UITableViewCell()
    }
    
    
}
