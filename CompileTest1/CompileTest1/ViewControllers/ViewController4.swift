//
//  ViewController4.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class ViewController4: UIViewController {
    
    var vwScroll: TebScrollView!
    var vwStack: UIStackView!
    
    var txt1: TebPlainTextField!
    var txt2: TebPlainTextField!
    var txt3: TebPlainTextField!
    var txt4: TebPlainTextField!
    var txt5: TebPlainTextField!
    
    private var btnAction: ActionButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Test"
        
        vwScroll = TebScrollView(frame: CGRect.zero)
        //vwScroll.backgroundColor = Color.color2DarkGreen
        vwScroll.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 64, right: 0)
        self.view.addSubview(vwScroll)
        
        self.btnAction = ActionButton()
        self.btnAction.addTarget(self, action: #selector(tapAction(sender:)), for: .touchUpInside)
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }
        
        vwScroll.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalTo(btnAction.snp.top)
        }
        
        vwStack = UIStackView()
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .center
        vwStack.distribution = .fillProportionally
        vwStack.spacing = 10 // 60
        vwStack.axis = .vertical
        
        vwScroll.addSubview(vwStack)
        vwStack.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(vwScroll.snp.width)
            make.height.equalTo(vwScroll.snp.height).priority(900)
        }
        
        let vwSpacer1: UIView = UIView(frame: CGRect.zero)
        vwSpacer1.backgroundColor = UIColor.yellow
        vwStack.addArrangedSubview(vwSpacer1)
        vwSpacer1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(64)
        }
        
        self.txt1 = TebPlainTextField()
        self.txt1.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt1)
        self.txt1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt2 = TebPlainTextField()
        self.txt2.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt2)
        self.txt2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt3 = TebPlainTextField()
        self.txt3.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt3)
        self.txt3.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt4 = TebPlainTextField()
        self.txt4.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt4)
        self.txt4.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        self.txt5 = TebPlainTextField()
        self.txt5.validators?.append( MinLengthValidator(minLength: 5) )
        vwStack.addArrangedSubview(self.txt5)
        self.txt5.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwStack.snp.width)
        }
        
        let vwSpacer2: UIView = UIView(frame: CGRect.zero)
        vwSpacer2.backgroundColor = UIColor.yellow
        vwStack.addArrangedSubview(vwSpacer2)
        vwSpacer2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.height.equalTo(64)
        }

    }
    
    func tapAction(sender: TebPickerType1?){
        
        self.view.endEditing(true)
        let isValid =  self.vwScroll.isValid()
        if( isValid ){
            // komponentler valid
            // confirmation çağır vs
        }else{
            // komponentlerden en az biri valid değil ve ona scrollanıldı
            // herhangi bir şey yapmaya gerek yok
        }
        
    }
}
