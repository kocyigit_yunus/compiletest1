//
//  StackViewTestViewController.swift
//  CompileTest1
//
//  Created by Yunus Koçyiğit on 30/01/2017.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class StackViewTestViewController: UIViewController {
    
    var stackView: UIStackView!
    var spacerView1: UIView!
    var spacerView2: UIView!
    var spacerView3: UIView!
    var spacerView4: UIView!
    
    var imageView1: UIImageView!
    var label1: UILabel!
    var filledView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Stack View Test"
        self.view.backgroundColor = UIColor.red
        
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.spacing = 0 // 60
        stackView.axis = .vertical
        self.view.addSubview(stackView)
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
        }
        
        spacerView1 = UIView()
        spacerView1.backgroundColor = UIColor.lightGray
        stackView.addArrangedSubview(spacerView1)
        
        imageView1 = UIImageView()
        imageView1.backgroundColor = UIColor.yellow
        stackView.addArrangedSubview(imageView1)
        imageView1.snp.makeConstraints { (make) in
            make.height.equalTo(100)
        }
        
        spacerView2 = UIView()
        spacerView2.backgroundColor = UIColor.lightGray
        //stackView.addArrangedSubview(spacerView2)
        
        label1 = UILabel()
        label1.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        label1.numberOfLines = 0
        label1.backgroundColor = UIColor.yellow
        stackView.addArrangedSubview(label1)
        
        
        spacerView3 = UIView()
        spacerView3.backgroundColor = UIColor.lightGray
        //stackView.addArrangedSubview(spacerView3)
        
        filledView = UIView()
        filledView.backgroundColor = UIColor.yellow
        stackView.addArrangedSubview(filledView)
        filledView.snp.makeConstraints { (make) in
            make.height.equalTo(150)
        }
        
        spacerView4 = UIView()
        spacerView4.backgroundColor = UIColor.lightGray
        stackView.addArrangedSubview(spacerView4)
        
    }
    
}
