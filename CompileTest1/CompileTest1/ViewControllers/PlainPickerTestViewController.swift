//
//  ViewController4.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class PlainPickerTestViewController: UIViewController {
    
    var component2: TebPlainPicker!
    var component1: TebAmountTextField!
    
    private var btnAction: ActionButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Test View Controller 5"
        
        var vwStack: UIStackView = UIStackView();
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .fill
        vwStack.distribution = .fill
        vwStack.spacing = 10 // 60
        vwStack.axis = .horizontal
        self.view.addSubview(vwStack)
        
        self.component1 = TebAmountTextField()
        self.component1.titleText = "Amount"
        self.component1.updateProperties(animated: false)
        vwStack.addArrangedSubview(component1)
        
        // disabled
        self.component2 = TebPlainPicker()
        self.component2.paddingLeft = 0
        self.component2.validators?.append( RequiredValidator() )
        self.component2.titleText = "Hesap Seçiniz"
        self.component2.titleTextFilled = "Hesap"
        self.component2.titleTextDisabled = "Hesap Seçilemiyor"
        self.component2.rightImageName = "iconArrowDownColor"
        self.component2.selectedItemLabel.text = "TL"
        self.component2.updateProperties(animated: false)
        self.component2.updateComponentState(newState: .filled, animated: true)
        self.component2.addTarget(self, action: #selector(tapComponent2(sender:)), for: .touchUpInside)
        vwStack.addArrangedSubview(component2)
        component2.snp.makeConstraints { (make) in
            make.width.equalTo(100)
        }
        
        vwStack.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.top.equalTo(100)
            make.right.equalTo(0)
        }
        
        
    }
    
    
    func tapAction(sender: TebPickerType1?){
        
        /*
        self.view.endEditing(true)
        let isValid =  self.vwScroll.isValid()
        if( isValid ){
            // komponentler valid
            // confirmation çağır vs
        }else{
            // komponentlerden en az biri valid değil ve ona scrollanıldı
            // herhangi bir şey yapmaya gerek yok
        }
         */
    }
    
    func tapComponent2(sender: TebPlainPicker ){
        let text1 = "BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla"
        let text2 = "BLA bla bla bla"
        
        /*
        if( self.infoLabel1.text == text1 ){
            self.infoLabel1.updateText(text: text2, animated: false)
        }else{
            self.infoLabel1.updateText(text: text1, animated: true)
        }
         */
        
    }
}
