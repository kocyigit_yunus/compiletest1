//
//  ViewController4.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 13/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class AmountTextFieldViewController: UIViewController {
    
    var vwScroll: TebFormScrollView!
    var vwStack: UIStackView!
    
    var infoLabel1: TebFormInfoView!
    var infoLabel2: TebFormInfoView!
    var infoLabel3: TebFormInfoView!
    var infoLabel4: TebFormInfoView!
    var infoLabel5: TebFormInfoView!
    
    var component1: TebAmountTextField!
    var component2: TebAmountTextField!
    var component3: TebAmountTextField!
    var component4: TebAmountTextField!
    var component5: TebAmountTextField!
    
    private var btnAction: ActionButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Test View Controller 5"
        
        let longText = "Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı Hesap Adı"
        
        vwScroll = TebFormScrollView(frame: CGRect.zero)
        vwScroll.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 64, right: 0)
        //vwScroll.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.15)
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.addSubview(vwScroll)
        
        self.btnAction = ActionButton()
        self.btnAction.addTarget(self, action: #selector(tapAction(sender:)), for: .touchUpInside)
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
        }
        
        vwScroll.snp.makeConstraints { (make) in
            make.top.equalTo(self.topLayoutGuide.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(btnAction.snp.top)
        }
        
        vwStack = UIStackView()
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .fill
        vwStack.distribution = .equalSpacing
        vwStack.spacing = 10 // 60
        vwStack.axis = .vertical
        
        vwScroll.addSubview(vwStack)
        vwStack.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(vwScroll.snp.width)
            make.height.greaterThanOrEqualTo(100)
            //make.height.greaterThanOrEqualTo(vwScroll.snp.height).priority(1000)
        }
        
        self.infoLabel1 = TebFormInfoView()
        self.infoLabel1.text = "Disabled"
        vwStack.addArrangedSubview(self.infoLabel1)
        
        // disabled
        self.component1 = TebAmountTextField()
        self.component1.leftImageName = "iconHomeColor"
        self.component1.validators?.append( RequiredValidator() )
        self.component1.titleText = "Hesap Adı"
        self.component1.titleTextDisabled = "Hesap Adı Yazılamıyor"
        self.component1.updateProperties(animated: false)
        self.component1.updateComponentState(newState: .emptyDisabled, animated: false)
        self.component1.addTarget(self, action: #selector(tapComponent1(sender:)), for: .touchUpInside)
        vwStack.addArrangedSubview(self.component1)
        
        self.infoLabel2 = TebFormInfoView()
        self.infoLabel2.text = "Empty"
        vwStack.addArrangedSubview(self.infoLabel2)
        
        // empty
        self.component2 = TebAmountTextField()
        self.component2.leftImageName = "iconHomeBlack"
        self.component2.validators?.append( RequiredValidator() )
        //self.component2.titleText = "Hesap Adı"
        self.component2.titleText = longText
        self.component2.updateProperties(animated: false)
        self.component2.updateComponentState(newState: .empty, animated: false)
        vwStack.addArrangedSubview(self.component2)
        
        self.infoLabel3 = TebFormInfoView()
        self.infoLabel3.text = "Empty Invalid"
        vwStack.addArrangedSubview(self.infoLabel3)
        
        // empty invalid
        self.component3 = TebAmountTextField()
        self.component3.leftImageName = "iconHomeBlack"
        self.component3.validators?.append( RequiredValidator() )
        self.component3.titleText = longText
        self.component3.updateProperties(animated: false)
        self.component3.validate()
        self.component3.updateComponentStateForValidation()
        vwStack.addArrangedSubview(self.component3)
        
        self.infoLabel4 = TebFormInfoView()
        self.infoLabel4.text = "Filled"
        vwStack.addArrangedSubview(self.infoLabel4)
        
        // filled
        self.component4 = TebAmountTextField()
        self.component4.leftImageName = "iconHomeBlack"
        self.component4.validators?.append( RequiredValidator() )
        self.component4.titleTextFilled = "Hesap Adı"
        self.component4.titleText = longText
        self.component4.inputTextField.text = "Hesap 1 - İstanbul - Teb"
        self.component4.updateProperties(animated: false)
        self.component4.updateComponentState(newState: .filled, animated: false)
        vwStack.addArrangedSubview(self.component4)
        
        self.infoLabel5 = TebFormInfoView()
        self.infoLabel5.text = "Filled Invalid"
        vwStack.addArrangedSubview(self.infoLabel5)
        
        //filled invalid
        self.component5 = TebAmountTextField()
        self.component5.leftImageName = "iconHomeBlack"
        self.component5.validators?.append( RequiredValidator() )
        self.component5.titleText = "Hesap Adı"
        self.component5.titleTextFilled = "Hesap Adı"
        self.component5.titleText = longText
        self.component5.inputTextField.text = "Hesap 1 - İstanbul - Teb"
        self.component5.invalidDescriptionLabel.text = "Hata Açıklaması"
        self.component5.updateProperties(animated: false)
        self.component5.updateComponentState(newState: .filledInvalid, animated: false)
        vwStack.addArrangedSubview(self.component5)
        
    }
    
    func tapAction(sender: TebPickerType1?){
        
        self.view.endEditing(true)
        let isValid =  self.vwScroll.isValid()
        if( isValid ){
            // komponentler valid
            // confirmation çağır vs
        }else{
            // komponentlerden en az biri valid değil ve ona scrollanıldı
            // herhangi bir şey yapmaya gerek yok
        }
    }
    
    func tapComponent1(sender: TebPlainPicker ){
        let text1 = "BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla BLA bla bla bla"
        let text2 = "BLA bla bla bla"
        
        if( self.infoLabel1.text == text1 ){
            self.infoLabel1.updateText(text: text2, animated: false)
        }else{
            self.infoLabel1.updateText(text: text1, animated: true)
        }
    }
}
