//
//  ViewController2.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 06/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class ViewController2: BaseViewController {
    
    var vwScroll: TebFormScrollView!
    var vwStack: UIStackView!
    
    var pickerA: TebPickerType1!
    var pickerB: TebPickerType1!
    var pickerC: TebPickerType1!
    var pickerD: TebPickerType1!
    var pickerE: TebPickerType1!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
        //self.view.backgroundColor = Color.colorDark40
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Test"
        
        vwScroll = TebFormScrollView(frame: CGRect.zero)
        //vwScroll.backgroundColor = Color.color2DarkGreen
        vwScroll.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 50, right: 0)
        self.view.addSubview(vwScroll)
        vwScroll.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.size.equalToSuperview()
        }
        
        vwStack = UIStackView()
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .center
        vwStack.distribution = .fillProportionally
        vwStack.spacing = 60
        vwStack.axis = .vertical

        vwScroll.addSubview(vwStack)
        vwStack.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.bottom.equalTo(0)
            make.width.equalTo(vwScroll.snp.width)
            make.height.equalTo(vwScroll.snp.height).priority(900)
        }
        
        pickerA = TebPickerType1()
        vwStack.addArrangedSubview(pickerA)
        pickerA.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        pickerB = TebPickerType1()
        pickerB.addTarget(self, action: #selector(tapPickerB(sender:)), for: .touchUpInside)
        vwStack.addArrangedSubview(pickerB)
        pickerB.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        pickerC = TebPickerType1()
        vwStack.addArrangedSubview(pickerC)
        pickerC.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        pickerD = TebPickerType1()
        vwStack.addArrangedSubview(pickerD)
        pickerD.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        pickerE = TebPickerType1()
        vwStack.addArrangedSubview(pickerE)
        pickerE.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
    }
    
    
    func tapPickerB(sender: TebPickerType1?){
        print("deneme")
         if(pickerB.pickerState == .invalid) {
            
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerB.updateState(state: .empty, animated: false)
                self.pickerC.updateState(state: .empty, animated: false)
                self.vwStack.removeArrangedSubview(self.pickerE)
                self.vwStack.addArrangedSubview(self.pickerE)
                self.vwScroll.layoutIfNeeded()
            })
            
         }else {
            //pickerB.pickerState = .selected
            
            UIView.animate(withDuration: 0.2, animations: {
                self.pickerB.updateState(state: .invalid, animated: false)
                self.pickerC.updateState(state: .invalid, animated: false)
                self.vwStack.removeArrangedSubview(self.pickerE)
                self.vwStack.addArrangedSubview(self.pickerE)
                self.vwScroll.layoutIfNeeded()
                self.vwScroll.contentOffset = CGPoint(x: 0, y: 50)
            })
            
         }
    }
    
}
