//
//  ViewController3.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 06/01/17.
//  Copyright © 2017 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

class ViewController3: BaseViewController {
    
    var vwScroll: TebScrollView!
    var vwStack: UIStackView!
    
    var pickerA: TebPickerType1!
    var pickerB: TebPickerType1!
    var pickerC: TebPickerType1!
    var pickerD: TebPickerType1!
    var pickerE: TebPickerType1!
    
    private var btnAction: ActionButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        self.view = UIView(frame: CGRect.zero)
        //self.view.backgroundColor = Color.colorDark40
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Test"
        
        
        
        vwScroll = TebScrollView(frame: CGRect.zero)
        //vwScroll.backgroundColor = Color.color2DarkGreen
        vwScroll.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 50, right: 0)
        self.view.addSubview(vwScroll)
        
        self.btnAction = ActionButton()
        self.view.addSubview(self.btnAction)
        self.btnAction.addTarget(self, action: #selector(tapAction(sender:)), for: .touchUpInside)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }
        
        vwScroll.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalTo(btnAction.snp.top)
        }
        
        vwStack = UIStackView()
        vwStack.translatesAutoresizingMaskIntoConstraints = false
        vwStack.alignment = .center
        vwStack.distribution = .fillProportionally
        vwStack.spacing = 10 // 60
        vwStack.axis = .vertical
        
        vwScroll.addSubview(vwStack)
        vwStack.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.equalTo(0)
            make.bottom.equalTo(0)
            make.right.equalTo(0)
            make.width.equalTo(vwScroll.snp.width)
            make.height.equalTo(vwScroll.snp.height).priority(900)
        }
        
        self.pickerA = TebPickerType1()
        self.pickerA.tag = 101
        self.pickerA.updateState(state: .emptyDisabled, animated: false)
        vwStack.addArrangedSubview(pickerA)
        pickerA.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            //make.width.equalTo(300)
        }
        
        self.pickerB = TebPickerType1()
        self.pickerB.tag = 102
        self.pickerB.validators?.append( RequiredValidator(errorMessage: "Picker 2 Zorunludur!" ))
        self.pickerB.preserveInvalidStateSpace = false
        self.pickerB.updateState(state: .empty, animated: false)
        //pickerB.preserveInvalidStateSpace = false
        //self.pickerB.updateState(state: .selectedInvalid, animated: false)
        pickerB.addTarget(self, action: #selector(tapPickerB(sender:)), for: .touchUpInside)
        vwStack.addArrangedSubview(pickerB)
        pickerB.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        self.pickerC = TebPickerType1()
        self.pickerC.tag = 103
        self.pickerC.updateState(state: .selected, animated: false)
        //pickerC.preserveInvalidStateSpace = false
        //self.pickerC.updateState(state: .selectedInvalid, animated: false)
        vwStack.addArrangedSubview(pickerC)
        pickerC.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
        pickerD = TebPickerType1()
        pickerD.tag = 104
        self.pickerD.updateState(state: .empty, animated: false)
        self.pickerD.validators?.append( RequiredValidator(errorMessage: "Picker 4 zorunludur!" ))
        vwStack.addArrangedSubview(pickerD)
        pickerD.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }

        pickerE = TebPickerType1()
        pickerE.tag = 105
        vwStack.addArrangedSubview(pickerE)
        pickerE.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
        }
        
    }
    
    
    
    func tapAction(sender: TebPickerType1?){
        
        
        self.view.endEditing(true)
        let isValid =  self.vwScroll.isValid()
        if( isValid ){
            // komponentler valid
            // confirmation çağır vs
        }else{
            // komponentlerden en az biri valid değil ve ona scrollanıldı
            // herhangi bir şey yapmaya gerek yok
        }
        
    }
    
    func tapPickerB(sender: TebPickerType1?){
        print("deneme")
        if(pickerB.pickerState == .invalid) {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.pickerB.updateState(state: .empty, animated: false)
                self.pickerD.updateState(state: .empty, animated: false)
                self.vwScroll.layoutIfNeeded()
            })
            
        }else {
            //pickerB.pickerState = .selected
            
            UIView.animate(withDuration: 0.3, animations: {
                self.pickerB.updateState(state: .invalid, animated: false)
                self.pickerD.updateState(state: .invalid, animated: false)
                self.vwScroll.layoutIfNeeded()
                //self.vwScroll.contentOffset = CGPoint(x: 0, y: 50)
            })
            
        }
    }
    
}
