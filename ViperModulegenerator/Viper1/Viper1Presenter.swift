//
//  Viper1Presenter.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import Foundation

protocol Viper1ViewOutput {
  
}

class Viper1Presenter: BasePresenter {

	typealias V = Viper1View
	typealias I = AnyObject
	typealias R = Viper1RouterInput

	weak var view: V?
	var interactor: I?
	var router: R?
}

extension Viper1Presenter: Viper1ViewOutput {

}