//
//  Viper1Router.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol Viper1RouterInput: RouterInput {

}

class Viper1Router: BaseRouter {
	
	typealias VC = Viper1ViewController
	weak var viewController: VC!

	struct Segues {
	}

	class func configure(_ viewController: Viper1ViewController) {
	
		let router = Viper1Router()
		let presenter = Viper1Presenter()

		viewController.router = router
		router.viewController = viewController
		presenter.router = router
		presenter.view = viewController
		viewController.presenter = presenter
	}
}

extension Viper1Router: Viper1RouterInput {

}