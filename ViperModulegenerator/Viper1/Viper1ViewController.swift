//
//  Viper1ViewController.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol Viper1View: ViewInput {

}

class Viper1ViewController: BaseViewController, BaseView {

	typealias P = Viper1ViewOutput
	typealias R = Viper1RouterInput

	var presenter: P?
	var router: R?

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func configureViper() {
        Viper1Router.configure(self)
    }
}

extension Viper1ViewController: Viper1View {

}