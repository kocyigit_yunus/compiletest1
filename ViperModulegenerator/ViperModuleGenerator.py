import sys
import os

template_view_controller = "TemplateViewController.txt"
template_interactor = "TemplateInteractor.txt"
template_presenter = "TemplatePresenter.txt"
template_router = "TemplateRouter.txt"

def createDiretory(moduleName):
	if not os.path.exists(moduleName): os.makedirs(moduleName)

def createFile(moduleName, fileName, content):
	fullName = "{0}/{1}.swift".format(moduleName, fileName)
	with open(fullName, 'w+') as file:
		file.write(content)

if __name__ == "__main__":
	moduleName = sys.argv[1]

	print "Creating module... ",
	createDiretory(moduleName)
	print"Done."

	print "Reading templates... ",
	tvc = open(template_view_controller).read()
	ti = open(template_interactor).read()
	tp = open(template_presenter).read()
	tr = open(template_router).read()
	print "Done. "

	print "Formatting files... ",
	tvc = tvc.replace("{0}", moduleName)
	ti = ti.replace("{0}", moduleName)
	tp = tp.replace("{0}", moduleName)
	tr = tr.replace("{0}", moduleName)
	print "Done."

	print "Creating view controller... ",
	fileName = "{0}ViewController".format(moduleName)
	createFile(moduleName, fileName, tvc)
	print "Done."
	#print "Creating interactor... ",
	#fileName = "{0}Interactor".format(moduleName)
	#createFile(moduleName, fileName, ti)
	print "Done."
	print "Creating presenter... ",
	fileName = "{0}Presenter".format(moduleName)
	createFile(moduleName, fileName, tp)
	print "Done."
	print "Creating router... ",
	fileName = "{0}Router".format(moduleName)
	createFile(moduleName, fileName, tr)
	print "Done."
	print "All Done."
