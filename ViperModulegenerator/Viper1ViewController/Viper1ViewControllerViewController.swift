//
//  Viper1ViewControllerViewController.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol Viper1ViewControllerView: ViewInput {

}

class Viper1ViewControllerViewController: BaseViewController, BaseView {

	typealias P = Viper1ViewControllerViewOutput
	typealias R = Viper1ViewControllerRouterInput

	var presenter: P?
	var router: R?

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func configureViper() {
        Viper1ViewControllerRouter.configure(self)
    }
}

extension Viper1ViewControllerViewController: Viper1ViewControllerView {

}