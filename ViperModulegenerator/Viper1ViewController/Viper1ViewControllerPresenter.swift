//
//  Viper1ViewControllerPresenter.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import Foundation

protocol Viper1ViewControllerViewOutput {
  
}

class Viper1ViewControllerPresenter: BasePresenter {

	typealias V = Viper1ViewControllerView
	typealias I = AnyObject
	typealias R = Viper1ViewControllerRouterInput

	weak var view: V?
	var interactor: I?
	var router: R?
}

extension Viper1ViewControllerPresenter: Viper1ViewControllerViewOutput {

}