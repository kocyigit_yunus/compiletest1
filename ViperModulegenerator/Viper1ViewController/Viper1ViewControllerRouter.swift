//
//  Viper1ViewControllerRouter.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol Viper1ViewControllerRouterInput: RouterInput {

}

class Viper1ViewControllerRouter: BaseRouter {
	
	typealias VC = Viper1ViewControllerViewController
	weak var viewController: VC!

	struct Segues {
	}

	class func configure(_ viewController: Viper1ViewControllerViewController) {
	
		let router = Viper1ViewControllerRouter()
		let presenter = Viper1ViewControllerPresenter()

		viewController.router = router
		router.viewController = viewController
		presenter.router = router
		presenter.view = viewController
		viewController.presenter = presenter
	}
}

extension Viper1ViewControllerRouter: Viper1ViewControllerRouterInput {

}