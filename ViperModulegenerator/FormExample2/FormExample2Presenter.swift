//
//  FormExample2Presenter.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import Foundation

protocol FormExample2ViewOutput {
  
}

class FormExample2Presenter: BasePresenter {

	typealias V = FormExample2View
	typealias I = AnyObject
	typealias R = FormExample2RouterInput

	weak var view: V?
	var interactor: I?
	var router: R?
}

extension FormExample2Presenter: FormExample2ViewOutput {

}