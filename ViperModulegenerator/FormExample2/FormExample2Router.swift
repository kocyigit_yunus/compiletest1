//
//  FormExample2Router.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol FormExample2RouterInput: RouterInput {

}

class FormExample2Router: BaseRouter {
	
	typealias VC = FormExample2ViewController
	weak var viewController: VC!

	struct Segues {
	}

	class func configure(_ viewController: FormExample2ViewController) {
	
		let router = FormExample2Router()
		let presenter = FormExample2Presenter()

		viewController.router = router
		router.viewController = viewController
		presenter.router = router
		presenter.view = viewController
		viewController.presenter = presenter
	}
}

extension FormExample2Router: FormExample2RouterInput {

}