//
//  FormExample2ViewController.swift
//  Cepteteb
//
//  Created by Viper Module Generator | V = 0.2
//  Copyright © 2016 TEB A.Ş. All rights reserved.
//

import UIKit

protocol FormExample2View: ViewInput {

}

class FormExample2ViewController: BaseViewController, BaseView {

	typealias P = FormExample2ViewOutput
	typealias R = FormExample2RouterInput

	var presenter: P?
	var router: R?

	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
	}

	override func configureViper() {
        FormExample2Router.configure(self)
    }
}

extension FormExample2ViewController: FormExample2View {

}