//
//  Viper27ViewController.swift
//  CompileTest27
//
//  Created by yunus koçyigit on 027/027/277.
//  Copyright © 20277 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

protocol Viper27View {

}

class Viper27ViewController: BaseViewController, Viper27View {

    typealias P = Viper27ViewOutput
    typealias R = Viper27RouterInput

    var presenter: P?
    var router: R?

    private var labelA: UILabel!
    private var labelB: UILabel!
    private var labelC: UILabel!

    private var pickerA: PickerType2!
    private var pickerB: PickerType2!
    private var pickerC: PickerType3!
    private var btnAction: ActionButton!
    private var txtField: UITextField!

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.pickerA = PickerType2()
        //pickerA.leftPadding = 0
        //pickerA.rightPadding = 0
        //pickerA.remakeConstraints()
        //pickerA.backgroundColor = UIColor.red
        //pickerA.pickerState = .selected
        self.view.addSubview(pickerA)
        pickerA.imageLeftEmpty = "wallet_black"
        pickerA.imageLeftSelected = "wallet_green"
        self.pickerA.addTarget(self, action:#selector(test(sender:)), for: .touchUpInside)

        pickerA.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalToSuperview().offset(0)
            make.height.greaterThanOrEqualTo(pickerA.minHeight)
        }

        self.pickerB = PickerType2()
        //self.pickerB.backgroundColor = UIColor.blue
        self.view.addSubview(pickerB)

        pickerB.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(pickerA.snp.bottom)
            make.height.greaterThanOrEqualTo(pickerB.minHeight)
        }

        /*
         self.pickerC = PickerType3()
         self.view.addSubview(self.self.pickerC)
         pickerC.snp.makeConstraints { (make) in
         make.left.equalTo(0)
         make.right.equalTo(0)
         make.top.equalTo(pickerB.snp.bottom).offset(0)
         }
         */

        self.btnAction = ActionButton()
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }

        self.txtField = UITextField(frame: CGRect.zero)
        self.txtField.backgroundColor = UIColor.red
        self.view.addSubview(self.txtField)
        self.txtField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(pickerB.snp.bottom)
        }


        let gr = UITapGestureRecognizer(target: self, action:#selector(backgroundTapped(sender:)) )
        self.view.addGestureRecognizer(gr)

    }

    func backgroundTapped( sender: AnyObject?){
        self.view.endEditing(true)
    }

    func test(sender : AnyObject?){
        print("deneme")
        if(pickerA.pickerState == .selected) {
            pickerA.pickerState = .empty
        }else {
            pickerA.pickerState = .selected
        }
        //picker27.pickerState = .selected
    }

    func configureViper() {
        //Viper27Router.configure(self)
    }

}

extension Viper27ViewController {

}

protocol Viper27RouterInput {

}

class Viper27Router: Viper27RouterInput{

    typealias VC = Viper27ViewController
    weak var viewController: VC!

    struct Segues {
    }

    class func configure(_ viewController: Viper27ViewController) {

        let router = Viper27Router()
        let presenter = Viper27Presenter()

        viewController.router = router
        router.viewController = viewController
        presenter.router = router
        //presenter.view = viewController
        viewController.presenter = presenter
    }
}

extension Viper27Router {


}

protocol Viper27ViewOutput {

}

class Viper27Presenter: Viper27ViewOutput {

    typealias V = Viper27View
    typealias I = AnyObject
    typealias R = Viper27RouterInput

    //weak var view: V?
    var interactor: I?
    var router: R?
}

extension Viper27Presenter {

}
