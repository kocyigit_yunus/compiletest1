//
//  Viper175ViewController.swift
//  CompileTest175
//
//  Created by yunus koçyigit on 0175/0175/1757.
//  Copyright © 201757 yunus koçyigit. All rights reserved.
//

import Foundation
import UIKit

protocol Viper175View {

}

class Viper175ViewController: BaseViewController, Viper175View {

    typealias P = Viper175ViewOutput
    typealias R = Viper175RouterInput

    var presenter: P?
    var router: R?

    private var labelA: UILabel!
    private var labelB: UILabel!
    private var labelC: UILabel!

    private var pickerA: PickerType2!
    private var pickerB: PickerType2!
    private var pickerC: PickerType3!
    private var btnAction: ActionButton!
    private var txtField: UITextField!

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.pickerA = PickerType2()
        //pickerA.leftPadding = 0
        //pickerA.rightPadding = 0
        //pickerA.remakeConstraints()
        //pickerA.backgroundColor = UIColor.red
        //pickerA.pickerState = .selected
        self.view.addSubview(pickerA)
        pickerA.imageLeftEmpty = "wallet_black"
        pickerA.imageLeftSelected = "wallet_green"
        self.pickerA.addTarget(self, action:#selector(test(sender:)), for: .touchUpInside)

        pickerA.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalToSuperview().offset(0)
            make.height.greaterThanOrEqualTo(pickerA.minHeight)
        }

        self.pickerB = PickerType2()
        //self.pickerB.backgroundColor = UIColor.blue
        self.view.addSubview(pickerB)

        pickerB.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(pickerA.snp.bottom)
            make.height.greaterThanOrEqualTo(pickerB.minHeight)
        }

        /*
         self.pickerC = PickerType3()
         self.view.addSubview(self.self.pickerC)
         pickerC.snp.makeConstraints { (make) in
         make.left.equalTo(0)
         make.right.equalTo(0)
         make.top.equalTo(pickerB.snp.bottom).offset(0)
         }
         */

        self.btnAction = ActionButton()
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }

        self.txtField = UITextField(frame: CGRect.zero)
        self.txtField.backgroundColor = UIColor.red
        self.view.addSubview(self.txtField)
        self.txtField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(pickerB.snp.bottom)
        }


        let gr = UITapGestureRecognizer(target: self, action:#selector(backgroundTapped(sender:)) )
        self.view.addGestureRecognizer(gr)

    }

    func backgroundTapped( sender: AnyObject?){
        self.view.endEditing(true)
    }

    func test(sender : AnyObject?){
        print("deneme")
        if(pickerA.pickerState == .selected) {
            pickerA.pickerState = .empty
        }else {
            pickerA.pickerState = .selected
        }
        //picker175.pickerState = .selected
    }

    func configureViper() {
        //Viper175Router.configure(self)
    }

}

extension Viper175ViewController {

}

protocol Viper175RouterInput {

}

class Viper175Router: Viper175RouterInput{

    typealias VC = Viper175ViewController
    weak var viewController: VC!

    struct Segues {
    }

    class func configure(_ viewController: Viper175ViewController) {

        let router = Viper175Router()
        let presenter = Viper175Presenter()

        viewController.router = router
        router.viewController = viewController
        presenter.router = router
        //presenter.view = viewController
        viewController.presenter = presenter
    }
}

extension Viper175Router {


}

protocol Viper175ViewOutput {

}

class Viper175Presenter: Viper175ViewOutput {

    typealias V = Viper175View
    typealias I = AnyObject
    typealias R = Viper175RouterInput

    //weak var view: V?
    var interactor: I?
    var router: R?
}

extension Viper175Presenter {

}
