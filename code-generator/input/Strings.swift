//
//  Strings.swift
//  CompileTest1
//
//  Created by yunus koçyigit on 29/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import Foundation

class Strings {

    func getLocalizedString(key : String) -> String {
        return key
    }

    //static let KEY_{{this}}: String = "{{this}} bla bla bla bla bla bla bla"
    //static var KEY_{{this}} : String { get { return "{{this}} bla bla bla bla bla bla bla" } }
    {{#each keys}}
    static var KEY_{{this}} : String { get { return "{{this}} bla bla bla bla bla bla bla" } }
    {{/each}}

}
