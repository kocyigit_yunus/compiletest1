//
//  ViewController{{key}}.swift
//  CompileTest{{key}}
//
//  Created by yunus koçyigit on 29/12/16.
//  Copyright © 2016 yunus koçyigit. All rights reserved.
//

import UIKit

class ViewController{{key}}: BaseViewController {

    private var label1: UILabel!
    private var label2: UILabel!
    private var label3: UILabel!

    private var picker1: PickerType2!
    private var picker2: PickerType2!
    private var picker3: PickerType3!
    private var btnAction: ActionButton!
    private var txtField: UITextField!

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    override func loadView() {
        self.view = UIView(frame: UIScreen.main.bounds)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.picker1 = PickerType2()
        //picker1.leftPadding = 0
        //picker1.rightPadding = 0
        //picker1.remakeConstraints()
        //picker1.backgroundColor = UIColor.red
        //picker1.pickerState = .selected
        self.view.addSubview(picker1)
        picker1.imageLeftEmpty = "wallet_black"
        picker1.imageLeftSelected = "wallet_green"
        self.picker1.addTarget(self, action:#selector(test(sender:)), for: .touchUpInside)

        picker1.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalToSuperview().offset(0)
            make.height.greaterThanOrEqualTo(picker1.minHeight)
        }

        self.picker2 = PickerType2()
        //self.picker2.backgroundColor = UIColor.blue
        self.view.addSubview(picker2)

        picker2.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(picker1.snp.bottom)
            make.height.greaterThanOrEqualTo(picker2.minHeight)
        }

        /*
        self.picker3 = PickerType3()
        self.view.addSubview(self.picker3)
        picker3.snp.makeConstraints { (make) in
            make.left.equalTo(0)
            make.right.equalTo(0)
            make.top.equalTo(picker2.snp.bottom).offset(0)
        }
 */

        self.btnAction = ActionButton()
        self.view.addSubview(self.btnAction)
        self.btnAction.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(btnAction.minHeight)
            make.bottom.equalTo(0)
            //make.bottom.equalToSuperview()
            //make.bottomMargin.equalToSuperview()
        }

        self.txtField = UITextField(frame: CGRect.zero)
        self.txtField.backgroundColor = UIColor.red
        self.view.addSubview(self.txtField)
        self.txtField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.top.equalTo(picker2.snp.bottom)
        }


        let gr = UITapGestureRecognizer(target: self, action:#selector(backgroundTapped(sender:)) )
        self.view.addGestureRecognizer(gr)
    }

    func backgroundTapped( sender: AnyObject?){
        self.view.endEditing(true)
    }

    func test(sender : AnyObject?){
        print("deneme")
        if(picker1.pickerState == .selected) {
            picker1.pickerState = .empty
        }else {
            picker1.pickerState = .selected
        }
        //picker1.pickerState = .selected
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
