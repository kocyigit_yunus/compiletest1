var fs = require('fs');
var handlebars = require('handlebars');
var outputFolder = "./output/Strings/"

var source = fs.readFileSync("./input/Strings.swift", "utf8");

/*
handlebars.registerHelper('list', function(items, options) {
  var out = "<ul>";
  for(var i=0, l=items.length; i<l; i++) {
    out = out + "<li>" + options.fn(items[i]) + "</li>";
  }
  return out + "</ul>";
});
*/

var template = handlebars.compile(source);
var data = { keys : [] };
for(var i=1; i<1001; i++){
  data.keys.push( "" + i );
}
data.abc = ['12','232','23132'];
//console.log(data);
var output = template(data);
console.log(output);

fs.writeFileSync(outputFolder + 'Strings.swift', output, 'utf-8');
console.log('Done!');
